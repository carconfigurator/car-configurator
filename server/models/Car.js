const keystone = require('keystone');
const Types = keystone.Field.Types;

// Create a new Keystone list called Car
const Car = new keystone.List('Car', {
  autokey: {
    path: 'slug',
    from: 'name',
    unique: true
  },
  defaultSort: '-createdAt'
});

const carImgStorage = new keystone.Storage({
  adapter: keystone.Storage.Adapters.FS,
  fs: {
    // required; path where the files should be stored
    path: keystone.expandPath('server/public/img'),
    generateFilename: function (file, index) {
      return file.filename;
    },
    whenExists: 'error',
    // path where files will be served
    publicPath: '/public/img/'
  }
});

// Finally we are gonna add the fields for our Car
Car.add({
  name: {
    type: String,
    required: true
  },
  dealer: {
    type: Types.Relationship,
    ref: 'Dealer'
  },
  company: {
    type: Types.Relationship,
    ref: 'Company'
  },
  model: {
    type: Types.Relationship,
    ref: 'Model',
    refPath: 'cars'
  },
  series: {
    type: Types.Relationship,
    ref: 'Series'
  },
  engines: {
    type: Types.Relationship,
    ref: 'Engine',
    many: true
  },
  interior: {
    type: Types.Relationship,
    ref: 'Interior',
    many: true
  },
  exterior: {
    type: Types.Relationship,
    ref: 'Exterior',
    many: true
  },
  options: {
    type: Types.Relationship,
    ref: 'Option',
    many: true
  },
  image: {
    type: Types.File,
    storage: carImgStorage,
    mimetype: '.jpeg, .jpg, .gif, .svg'
  },
  images: {
    type: Types.CloudinaryImages,
    generateFilename: function (file, attemptNumber, callback) {
      let originalName = file.originalname;
      let filenameWithoutExtension = originalName.substring(0, originalName.lastIndexOf('.'));
      let timestamp = new Date().getTime();
      return `${filenameWithoutExtension}-${timestamp}`;
    }
  },
  price: {
    type: Number
  }
});

// Setting the default order of the columns on the admin tab
Car.defaultColumns = 'name';
Car.register();

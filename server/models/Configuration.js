const keystone = require('keystone');
const Types = keystone.Field.Types;

// Create a new Keystone list called Car
const Configuration = new keystone.List('Configuration', {
  autokey: {
    path: 'slug',
    from: 'name',
    unique: true
  },
  defaultSort: '-createdAt'
});

// Finally we are gonna add the fields for our Car
Configuration.add({
  name: {
    type: String,
    required: true
  },
  engines: {
    type: Types.Relationship,
    ref: 'Engine',
    many: true
  },
  interior: {
    type: Types.Relationship,
    ref: 'Interior',
    many: true
  },
  exterior: {
    type: Types.Relationship,
    ref: 'Exterior',
    many: true
  },
  options: {
    type: Types.Relationship,
    ref: 'Option',
    many: true
  },
  series: {
    type: Types.Relationship,
    ref: 'Series'
  }
});

// Setting the default order of the columns on the admin tab
Configuration.defaultColumns = 'name';
Configuration.register();

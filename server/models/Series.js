const keystone = require('keystone');
const Types = keystone.Field.Types;

// Create a new Keystone list called Series
const Series = new keystone.List('Series', {
  autokey: {
    path: 'slug',
    from: 'name',
    unique: true
  },
  defaultSort: '-createdAt'
});

const seriesImgStorage = new keystone.Storage({
  adapter: keystone.Storage.Adapters.FS,
  fs: {
    // required; path where the files should be stored
    path: keystone.expandPath('server/public/img'),
    generateFilename: function (file, index) {
      return file.filename;
    },
    whenExists: 'error',
    // path where files will be served
    publicPath: '/public/img/'
  }
});

// Finally we are gonna add the fields for our Series
Series.add({
  name: {
    type: String,
    required: true
  },
  model: {
    type: Types.Relationship,
    ref: 'Model'
  },
  startPrice: {
    type: Types.Number
  },
  image: {
    type: Types.File,
    storage: seriesImgStorage,
    mimetype: '.jpeg, .jpg, .gif, .svg'
  },
  description: {
    type: Types.TextArray
  }
});

// Setting the default order of the columns on the admin tab
Series.defaultColumns = 'name';
Series.register();

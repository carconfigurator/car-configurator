const keystone = require('keystone');
const Types = keystone.Field.Types;

// Create a new Keystone list called Car
const Option = new keystone.List('Option', {
  autokey: {
    path: 'slug',
    from: 'name',
    unique: true
  },
  defaultSort: '-createdAt'
});

const optionImgStorage = new keystone.Storage({
  adapter: keystone.Storage.Adapters.FS,
  fs: {
    // required; path where the files should be stored
    path: keystone.expandPath('server/public/img'),
    generateFilename: function (file, index) {
      return file.filename;
    },
    whenExists: 'error',
    // path where files will be served
    publicPath: '/public/img/'
  }
});

// Finally we are gonna add the fields for our Car
Option.add({
  name: {
    type: String,
    required: true
  },
  title: {
    type: String
  },
  bodyPrice: {
    type: Number
  },
  image: {
    type: Types.File,
    storage: optionImgStorage,
    mimetype: '.jpeg, .jpg, .gif, .svg'
  },
  fullDescription: {
    type: String
  },
  category: {
    type: Types.Relationship,
    ref: 'Category'
  },
  conflicts: {
    type: Types.Relationship,
    ref: 'Option',
    many: true
  },
  modelDescription: {
    type: Types.Textarea
  }
});

// Setting the default order of the columns on the admin tab
Option.defaultColumns = 'name';
Option.register();

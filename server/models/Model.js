const keystone = require('keystone');
const Types = keystone.Field.Types;

// Create a new Keystone list called Model
const Model = new keystone.List('Model', {
  autokey: {
    path: 'slug',
    from: 'name',
    unique: true
  },
  defaultSort: '-company'
});

// Adding the option to add an image to our Model from
const companyImgStorage = new keystone.Storage({
  adapter: keystone.Storage.Adapters.FS,
  fs: {
    // required; path where the files should be stored
    path: keystone.expandPath('server/public/img'),
    generateFilename: function (file, index) {
      return file.filename;
    },
    whenExists: 'error',
    // path where files will be served
    publicPath: '/public/img/'
  }
});

// Finally we are gonna add the fields for our Model
Model.add({
  name: {
    type: String,
    required: true
  },
  image: {
    type: Types.File,
    storage: companyImgStorage,

    mimetype: '.jpeg, .jpg, .gif, .svg'
  },
  startPrice: {
    type: Types.Number
  },
  company: {
    type: Types.Relationship,
    ref: 'Company'
  }
});

Model.relationship({ path: 'cars', ref: 'Car', refPath: 'model' });

// Setting the default order of the columns on the admin tab
Model.defaultColumns = 'name,company';
Model.register();

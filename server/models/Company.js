const keystone = require('keystone');
const Types = keystone.Field.Types;

// Create a new Keystone list called Company
const Company = new keystone.List('Company', {
  autokey: {
    path: 'slug',
    from: 'name',
    unique: true
  },
  defaultSort: '-createdAt'
});

// Adding the option to add an image to our Company from
const companyImgStorage = new keystone.Storage({
  adapter: keystone.Storage.Adapters.FS,
  fs: {
    // required; path where the files should be stored
    path: keystone.expandPath('server/public/img'),
    generateFilename: function (file, index) {
      return file.filename;
    },
    whenExists: 'error',
    // path where files will be served
    publicPath: '/public/img/'
  }
});

// Finally we are gonna add the fields for our Company
Company.add({
  name: {
    type: String,
    required: true
  },
  image: {
    type: Types.File,
    storage: companyImgStorage,
    mimetype: '.jpeg, .jpg, .gif, .svg'
  }
});

// Setting the default order of the columns on the admin tab
Company.defaultColumns = 'name';
Company.register();

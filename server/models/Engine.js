const keystone = require('keystone');
const Types = keystone.Field.Types;

// Create a new Keystone list called Car
const Engine = new keystone.List('Engine', {
  autokey: {
    path: 'slug',
    from: 'name',
    unique: true
  },
  defaultSort: '-createdAt'
});

const engineImgStorage = new keystone.Storage({
  adapter: keystone.Storage.Adapters.FS,
  fs: {
    // required; path where the files should be stored
    path: keystone.expandPath('server/public/img'),
    generateFilename: function (file, index) {
      return file.filename;
    },
    whenExists: 'error',
    // path where files will be served
    publicPath: '/public/img/'
  }
});

// Finally we are gonna add the fields for our Car
Engine.add({
  name: {
    type: String,
    required: true
  },
  title: {
    type: String
  },
  headerPrice: {
    type: Number
  },
  headerDescription: {
    type: String
  },
  fullDescription: {
    type: String
  },
  image: {
    type: Types.File,
    storage: engineImgStorage,
    mimetype: '.jpeg, .jpg, .gif, .svg'
  },
  additionalData: {
    type: Types.List,
    fields: {
      title: {
        type: String
      },
      value: {
        type: String
      }
    }
  },
  category: {
    type: Types.Relationship,
    ref: 'Category'
  },
  modelDescription: {
    type: Types.Textarea
  }
});

// Setting the default order of the columns on the admin tab
Engine.defaultColumns = 'name';
Engine.register();

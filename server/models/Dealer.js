const keystone = require('keystone');
const Types = keystone.Field.Types;

// Create a new Keystone list called Car
const Dealer = new keystone.List('Dealer', {
  autokey: {
    path: 'slug',
    from: 'name',
    unique: true
  },
  defaultSort: '-createdAt'
});

const dealerImgStorage = new keystone.Storage({
  adapter: keystone.Storage.Adapters.FS,
  fs: {
    // required; path where the files should be stored
    path: keystone.expandPath('server/public/img'),
    generateFilename: function (file, index) {
      return file.filename;
    },
    whenExists: 'error',
    // path where files will be served
    publicPath: '/public/img/'
  }
});

// Finally we are gonna add the fields for our Car
Dealer.add({
  name: {
    type: String,
    required: true
  },
  address: {
    type: String
  },
  phone: {
    type: Number
  },
  location: {
    type: Types.GeoPoint
  },
  image: {
    type: Types.File,
    storage: dealerImgStorage,
    mimetype: '.jpeg, .jpg, .gif, .svg'
  }
});

// Setting the default order of the columns on the admin tab
Dealer.defaultColumns = 'name';
Dealer.register();

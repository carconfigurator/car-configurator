const keystone = require('keystone');
const Types = keystone.Field.Types;

// Create a new Keystone list called Car
const Category = new keystone.List('Category', {
  autokey: {
    path: 'slug',
    from: 'name',
    unique: true
  },
  defaultSort: '-createdAt'
});

const categoryImgStorage = new keystone.Storage({
  adapter: keystone.Storage.Adapters.FS,
  fs: {
    // required; path where the files should be stored
    path: keystone.expandPath('server/public/img'),
    generateFilename: function (file, index) {
      return file.filename;
    },
    whenExists: 'error',
    // path where files will be served
    publicPath: '/public/img/'
  }
});

// Finally we are gonna add the fields for our Car
Category.add({
  name: {
    type: String,
    required: true
  },
  uniqueItems: {
    type: Types.Boolean
  },
  image: {
    type: Types.File,
    storage: categoryImgStorage,
    mimetype: '.jpeg, .jpg, .giF, .svg'
  }
});

// Setting the default order of the columns on the admin tab
Category.defaultColumns = 'name';
Category.register();

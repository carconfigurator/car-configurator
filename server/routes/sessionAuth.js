const keystone = require('keystone');

exports.signin = (req, res) => {

  if (!req.body.username || !req.body.password) {
    return res.json({success: false});
  }

  keystone.list('User').model.findOne({email: req.body.username}).exec(function (err, user) {

    if (err || !user) {
      return res.json({
        success: false,
        session: false,
        message: (err && err.message ? err.message : false) || 'Sorry, there was an issue signing you in, please try again.'
      });
    }

    keystone.session.signin({
      email: user.email,
      password: req.body.password
    }, req, res, function (user) {
      return res.json({
        success: true,
        session: true,
        date: new Date().getTime(),
        userId: user.id,
        name: req.user.name,
        dealers: req.user.dealers
      });

    }, function (err) {

      return res.json({
        success: true,
        session: false,
        message: (err && err.message ? err.message : false) || 'Sorry, there was an issue signing you in, please try again.'
      });

    });

  });
};

exports.checkAuth = (req, res, next) => {
  if (!req.user) {
    return res.json({
      success: !!req.user,
      error: 'Неправильный e-mail или пароль!'
    });
  }

  return res.json({
    name: req.user.name,
    dealers: req.user.dealers,
    success: !!req.user
  });
};

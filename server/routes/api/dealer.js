const keystone = require('keystone');

/**
 * List Dealer
 */

    // Getting our recipe model
const Dealer = keystone.list('Dealer');

// Creating the API end point
exports.list = function (req, res) {
  // Querying the data this works similarly to the Mongo db.collection.find() method
  Dealer.model.find().exec(function (err, items) {
    // Make sure we are handling errors
    if (err) {
      return res.apiError('database error', err);
    }
    res.apiResponse([
      // Filter recipe by
      ...items
    ]);

    // Using express req.query we can limit the number of recipes returned by setting a limit property in the link
    // This is handy if we want to speed up loading times once our recipe collection grows
  });
};

const keystone = require('keystone');
const async = require('async');

/**
 * List Model
 */

    // Getting our recipe model
const Model = keystone.list('Model');
const Car = keystone.list('Car');

// Creating the API end point
exports.list = function (req, res) {
  // Querying the data this works similarly to the Mongo db.collection.find() method
  let model = Model.model.find();
  if (req.body.company && req.body.company.match(/^[0-9a-fA-F]{24}$/)) {
    model.where('company', req.body.company);
  }
  model.sort({name: 'asc'}).populate('cars').exec(function (err, models) {
        if (err) {
          return res.apiError('database error', err);
        }

        async.transform(models, function (acc, item, index, callback) {
          process.nextTick(function () {
            Car.model.find({}, '_id').where('model', item._id).exec(function (err, cars) {
              acc[index] = {
                ...item.toObject(),
                cars
              };
              callback(null);
            });
          });
        }, function (err, result) {
          res.apiResponse(
              result
          );
        });

      }
  );
};

exports.find = function (req, res) {
  // Querying the data this works similarly to the Mongo db.collection.find() method
  Model.model.findOne().where('company', req.body.company).where('slug', req.body.model).exec(function (err, item) {
    // Make sure we are handling errors
    if (err) {
      return res.apiError('database error', err);
    }

    res.apiResponse(item);

    // Using express req.query we can limit the number of recipes returned by setting a limit property in the link
    // This is handy if we want to speed up loading times once our recipe collection grows
  });
};

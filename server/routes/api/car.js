const keystone = require('keystone');

/**
 * List Series
 */

    // Getting our recipe model
const Car = keystone.list('Car');

exports.list = function (req, res) {
  // Querying the data this works similarly to the Mongo db.collection.find() method
  let cars = Car.model.find();
  if (req.body.series) {
    cars.where('series', req.body.series);
  }
  if (req.body.dealers && req.body.dealers.length > 0) {
    cars.where('dealer').in(req.body.dealers)
  }
  cars.populate({
        path: 'company',
        select: 'name'
      })
      .populate({
        path: 'model',
        select: 'name'
      })
      .populate({
        path: 'series',
        select: 'name'
      })
      .populate({
        path: 'engines',
        select: '_id title category'
      })
      .populate({
        path: 'interior',
        select: '_id title category'
      })
      .populate({
        path: 'exterior',
        select: '_id title category'
      })
      .populate({
        path: 'options',
        select: '_id title category'
      })
      .populate({
        path: 'dealer'
      })
      .exec(function (err, items) {
        // Make sure we are handling errors
        if (err) {
          return res.apiError('database error', err);
        }

        res.apiResponse([
          // Filter recipe by
          ...items
        ]);

        // Using express req.query we can limit the number of recipes returned by setting a limit property in the link
        // This is handy if we want to speed up loading times once our recipe collection grows
      });
};

exports.find = function (req, res) {
  const {id} = req.body;
  if (!id.match(/^[0-9a-fA-F]{24}$/)) {
    return res.apiResponse({});
  }
  Car.model.findById(id).where('_id', req.body.id)
      .populate({
        path: 'company',
        select: 'name'
      })
      .populate({
        path: 'model',
        select: 'name'
      })
      .populate({
        path: 'series',
        select: 'name'
      })
      .populate({
        path: 'engines',
        select: '_id title category',
        populate: {
          path: 'category',
          select: 'uniqueItems'
        }
      })
      .populate({
        path: 'interior',
        select: '_id title category',
        populate: {
          path: 'category',
          select: 'uniqueItems'
        }
      })
      .populate({
        path: 'exterior',
        select: '_id title category',
        populate: {
          path: 'category',
          select: 'uniqueItems'
        }
      })
      .populate({
        path: 'options',
        select: '_id title category',
        populate: {
          path: 'category',
          select: 'uniqueItems'
        }
      })
      .populate({
        path: 'dealer'
      })
      .exec(function (err, item) {
        // Make sure we are handling errors
        if (err) {
          return res.apiError('database error', err);
        }

        res.apiResponse(item);

        // Using express req.query we can limit the number of recipes returned by setting a limit property in the link
        // This is handy if we want to speed up loading times once our recipe collection grows
      });
};

exports.getLast = function (req, res) {
  Car.model.find().limit(9).sort({$natural: -1}).populate({
        path: 'engines',
        select: '_id title category'
      })
      .populate({
        path: 'interior',
        select: '_id title category'
      })
      .populate({
        path: 'exterior',
        select: '_id title category'
      })
      .populate({
        path: 'options',
        select: '_id title category'
      })
      .populate({
        path: 'dealer'
      }).exec(function (err, items) {
    if (err) {
      return res.apiError('database error', err);
    }
    res.apiResponse([
      ...items
    ]);
  });
};

exports.editCar = function (req, res) {
  const {car} = req.body;
  const id = car._id;

  if (!id.match(/^[0-9a-fA-F]{24}$/)) {
    return res.apiResponse({});
  }
  Car.model.findById(id).where('_id', id).exec(function (err, item) {
    if (err) {
      return res.apiError('database error', err);
    }

    Object.keys(car).forEach(function (key) {
      item[key] = car[key];
    });

    item.save();

  });
  res.apiResponse({
    error: null,
    status: 'SUCCESS'
  });
};

exports.createCar = function (req, res) {
  /*
    const {car} = req.body;

    new Car.model(car).save();
  */

  res.apiResponse({
    error: null,
    status: 'SUCCESS'
  });
};

exports.deleteCar = function (req, res) {
  const {id} = req.body;
  if (!id) {
    res.apiResponse({
      error: 'Send ID First',
      status: 'ERROR'
    });
  }
  Car.model.findById(id).remove(function (error) {
    if (error) {
      res.apiResponse({
        error,
        status: 'ERROR'
      });
    } else {
      res.apiResponse({
        error: null,
        status: 'SUCCESS'
      });
    }
  });
};

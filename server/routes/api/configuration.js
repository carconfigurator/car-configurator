const keystone = require('keystone');

/**
 * List Series
 */

    // Getting our recipe model
const Configuration = keystone.list('Configuration');

exports.find = function (req, res) {
  // Querying the data this works similarly to the Mongo db.collection.find() method
  Configuration.model.findOne().where('series', req.body.series)
      .populate({
        path: 'engines',
        populate: {path: 'category'}
      })
      .populate({
        path: 'interior',
        populate: {path: 'category'}
      })
      .populate({
        path: 'exterior',
        populate: {path: 'category'}
      })
      .populate({
        path: 'options',
        populate: {path: 'category'}
      })
      .exec(function (err, item) {
        // Make sure we are handling errors
        if (err) {
          return res.apiError('database error', err);
        }

        const responseItem = item || {};

        res.apiResponse(responseItem);

        // Using express req.query we can limit the number of recipes returned by setting a limit property in the link
        // This is handy if we want to speed up loading times once our recipe collection grows
      });
};

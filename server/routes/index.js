const path = require('path');
const keystone = require('keystone');
// Then to get access to our API route we will use importer
const importRoutes = keystone.importer(__dirname);
// And finally set up the api on a route
const routes = {
  api: importRoutes('./api')
};
const sessionAuth = require('./sessionAuth');

const apiRoute = 'api';

// Export our app routes
exports = module.exports = function (app) {
  app.post(`/${apiRoute}/admin/signin`, keystone.middleware.cors, sessionAuth.signin);

  app.post(`/${apiRoute}/admin/authentication/`, keystone.security.csrf.middleware.init, function (req, res, next) {
    sessionAuth.checkAuth(req, res, next);
  });

  app.post(`/${apiRoute}/getCompanies/`, keystone.middleware.api, routes.api.company.list);
  app.post(`/${apiRoute}/findCompany/`, keystone.middleware.api, routes.api.company.find);

  app.post(`/${apiRoute}/getModels/`, keystone.middleware.api, routes.api.model.list);
  app.post(`/${apiRoute}/findModel/`, keystone.middleware.api, routes.api.model.find);

  app.post(`/${apiRoute}/getSeries/`, keystone.middleware.api, routes.api.series.list);
  app.post(`/${apiRoute}/findSeries/`, keystone.middleware.api, routes.api.series.find);

  app.post(`/${apiRoute}/findConfiguration/`, keystone.middleware.api, routes.api.configuration.find);

  app.post(`/${apiRoute}/getLastCars/`, keystone.middleware.api, routes.api.car.getLast);
  app.post(`/${apiRoute}/getCars/`, keystone.middleware.api, routes.api.car.list);
  app.post(`/${apiRoute}/findCar/`, keystone.middleware.api, routes.api.car.find);

  app.post(`/${apiRoute}/admin/editCar/`, keystone.middleware.api, routes.api.car.editCar);
  app.post(`/${apiRoute}/admin/createCar/`, keystone.middleware.api, routes.api.car.createCar);
  app.post(`/${apiRoute}/admin/deleteCar/`, keystone.middleware.api, routes.api.car.deleteCar);


  app.post(`/${apiRoute}/getDealers/`, keystone.middleware.api, routes.api.dealer.list);

  app.get('/*', function (req, res) {
    res.sendFile(path.join(__dirname, '../../client/build', 'index.html'));
  });
};
keystone.pre('routes', keystone.security.csrf.middleware.init);

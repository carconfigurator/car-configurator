import {ActionTypes, EStatus} from 'constant';
import {asyncReducer} from 'utils';

export const personAuthentication = (prevState = personAuthenticationInitState(), action) => {
  return asyncReducer(action, ActionTypes.CHECK_PERSON_AUTHENTICATION, prevState, personAuthenticationInitState);
};

const personAuthenticationInitState = () => ({
  status: EStatus.IDLE,
  data: {
    name: {
      first: '',
      last: ''
    },
    dealers: []
  }
});


import {ActionTypes} from 'constant';

export const personChoose = (prevState = personChooseInitState(), action) => {
  let newState = null;
  switch (action.type) {
    case ActionTypes.SET_PERSON_CHOOSE:
      newState = action.payload;
      break;
    case ActionTypes.CLEAR_PERSON_CHOOSE:
      newState = personChooseInitState();
      break;
    default:
      newState = prevState;
  }
  return newState || prevState;
};

const personChooseInitState = () => ({
  engines: [],
  exterior: [],
  interior: [],
  options: [],
  itemsId: []
});


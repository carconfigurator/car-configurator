import {combineReducers} from 'redux';

import {personChoose} from './personChooseReducer';
import {personAuthentication} from './personAuthentication';

export const personsReducer = combineReducers({
  personChoose,
  personAuthentication
});

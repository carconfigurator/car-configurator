import {asyncReducer} from 'utils';
import {ActionTypes, EStatus} from 'constant';

export const carsList = (prevState = getCarsInitState(), action) => {
  return asyncReducer(action, ActionTypes.GET_CARS_LIST, prevState, getCarsInitState);
};

const getCarsInitState = () => ({
  error: null,
  data: [],
  status: EStatus.IDLE
});

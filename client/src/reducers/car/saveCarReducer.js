import {asyncReducer} from 'utils';
import {ActionTypes, EStatus} from 'constant';

export const saveCar = (prevState = saveCarInitState(), action) => {
  return asyncReducer(action, ActionTypes.SAVE_CAR, prevState, saveCarInitState);
};

const saveCarInitState = () => ({
  error: null,
  status: EStatus.IDLE
});

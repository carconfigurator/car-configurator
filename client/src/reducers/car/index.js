import {combineReducers} from 'redux';
import {carsList} from './carListReducers';
import {filteredCars, activeCar} from './carReducers';
import {editCar} from './editCarReducer';
import {saveCar} from './saveCarReducer';
import {deleteCar} from './deleteCarReducer';

export const carsReducer = combineReducers({
  carsList,
  filteredCars,
  activeCar,
  editCar,
  saveCar,
  deleteCar
});

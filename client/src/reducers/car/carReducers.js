import {ActionTypes, SUCCESS} from 'constant';

export const filteredCars = (prevState = filteredCarsInitState(), action) => {
  let newState = null;
  switch (action.type) {
    case ActionTypes.GET_CARS_LIST + SUCCESS:
      newState = {
        suitable: [],
        similar: action.payload
      };
      break;
    case ActionTypes.FILTER_CARS:
      newState = action.payload;
      break;
    default:
      newState = prevState;
  }
  return newState || prevState;
};

export const activeCar = (prevState = {}, action) => {
  let newState = null;
  switch (action.type) {
    case ActionTypes.SET_ACTIVE_CAR:
    case ActionTypes.FIND_CAR + SUCCESS:
      newState = action.payload;
      break;
    default:
      newState = prevState;
  }
  return newState || prevState;
};

const filteredCarsInitState = () => ({
  suitable: [],
  similar: []
});


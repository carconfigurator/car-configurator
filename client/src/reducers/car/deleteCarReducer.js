import {asyncReducer} from 'utils';
import {ActionTypes, EStatus} from 'constant';

export const deleteCar = (prevState = deleteCarInitState(), action) => {
  return asyncReducer(action, ActionTypes.DELETE_CAR, prevState, deleteCarInitState);
};

const deleteCarInitState = () => ({
  error: null,
  status: EStatus.IDLE
});

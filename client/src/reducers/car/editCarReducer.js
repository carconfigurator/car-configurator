import {asyncReducer} from 'utils';
import {ActionTypes, EStatus} from 'constant';

export const editCar = (prevState = editCarInitState(), action) => {
  return asyncReducer(action, ActionTypes.EDIT_CAR, prevState, editCarInitState);
};

const editCarInitState = () => ({
  error: null,
  status: EStatus.IDLE
});

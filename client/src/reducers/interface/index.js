import {combineReducers} from 'redux';
import {confirmation} from './confirmationReducer';
import {page} from './pageReducers';

export const interfaceReducer = combineReducers({
  confirmation,
  page
});

import {ActionTypes} from 'constant';

export const page = (prevState = getPageInitState(), action) => {
  let newState = null;
  switch (action.type) {
    case ActionTypes.SET_ACTIVE_PAGE:
      newState = action.payload;
      break;
    default:
      newState = prevState;
  }
  return newState || prevState;
};

const getPageInitState = () => 'series';

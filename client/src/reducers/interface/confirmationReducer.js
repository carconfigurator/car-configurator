import {ActionTypes} from 'constant';

export const confirmation = (prevState = {}, action) => {
  let newState = null;

  switch (action.type) {
    case ActionTypes.OPEN_CONFIRMATION_MODAL:
      newState = action.payload;
      break;
    case ActionTypes.CLOSE_CONFIRMATION_MODAL:
      newState = {};
      break;
    default: {
      newState = prevState;
    }
  }

  return newState || prevState;
};

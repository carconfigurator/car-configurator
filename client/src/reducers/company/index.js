import {combineReducers} from 'redux';
import {companiesList} from './companyListReducer';
import {activeCompany} from './companyReducers';

export const companiesReducer = combineReducers({
  activeCompany,
  companiesList
});

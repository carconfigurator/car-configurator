import {ActionTypes, SUCCESS} from 'constant';

export const activeCompany = (prevState = {}, action) => {
  let newState = null;
  switch (action.type) {
    case ActionTypes.SET_COMPANY_DATA:
    case ActionTypes.GET_COMPANY + SUCCESS:
      newState = action.payload;
      break;
    default:
      newState = prevState;
  }
  return newState || prevState;
};


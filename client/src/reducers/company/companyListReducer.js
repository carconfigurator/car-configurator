import {asyncReducer} from 'utils';
import {ActionTypes, EStatus} from 'constant';

export const companiesList = (prevState = getCompaniesInitState(), action) => {
  return asyncReducer(action, ActionTypes.GET_COMPANIES_LIST, prevState, getCompaniesInitState);
};

const getCompaniesInitState = () => ({
  error: null,
  data: [],
  status: EStatus.IDLE
});

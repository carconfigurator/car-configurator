import {combineReducers} from 'redux';
import {seriesList} from './seriesListReducer';
import {activeSeries} from './seriesReducers';

export const seriesReducer = combineReducers({
  activeSeries,
  seriesList
});

import {ActionTypes, SUCCESS} from 'constant';

export const activeSeries = (prevState = {}, action) => {
  let newState = null;
  switch (action.type) {
    case ActionTypes.SET_SERIES_DATA:
    case ActionTypes.GET_SERIES + SUCCESS:
      newState = action.payload;
      break;
    default:
      newState = prevState;
  }
  return newState || prevState;
};


import {asyncReducer} from 'utils';
import {ActionTypes, EStatus} from 'constant';

export const seriesList = (prevState = getSeriesInitState(), action) => {
  return asyncReducer(action, ActionTypes.GET_SERIES_LIST, prevState, getSeriesInitState);
};

const getSeriesInitState = () => ({
  error: null,
  data: [],
  status: EStatus.IDLE
});

import {combineReducers} from 'redux';
import {modelsList} from './modelListReducer';
import {activeModel} from './modelReducers';

export const modelsReducer = combineReducers({
  activeModel,
  modelsList
});

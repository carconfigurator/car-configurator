import {ActionTypes, SUCCESS} from 'constant';

export const activeModel = (prevState = {}, action) => {
  let newState = null;
  switch (action.type) {
    case ActionTypes.SET_MODEL_DATA:
    case ActionTypes.GET_MODEL + SUCCESS:
      newState = action.payload;
      break;
    default:
      newState = prevState;
  }
  return newState || prevState;
};


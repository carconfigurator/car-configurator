import {asyncReducer} from 'utils';
import {ActionTypes, EStatus} from 'constant';

export const modelsList = (prevState = getModelsInitState(), action) => {
  return asyncReducer(action, ActionTypes.GET_MODELS_LIST, prevState, getModelsInitState);
};

const getModelsInitState = () => ({
  error: null,
  data: [],
  status: EStatus.IDLE
});

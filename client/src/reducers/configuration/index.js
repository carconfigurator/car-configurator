import {combineReducers} from 'redux';
import {activeConfiguration} from './activeConfiguration';

export const configurationsReducer = combineReducers({
  activeConfiguration
});

import {ActionTypes, SUCCESS} from 'constant';
import {sortConfiguration} from 'utils';

export const activeConfiguration = (prevState = activeConfigurationInitState(), action) => {
  let newState = null;
  switch (action.type) {
    case ActionTypes.GET_CONFIGURATION + SUCCESS:
      newState = sortConfiguration(action.payload);
      break;
    default:
      newState = prevState;
  }
  return newState || prevState;
};

const activeConfigurationInitState = () => ({
  engines: [],
  exterior: [],
  interior: [],
  options: []
});


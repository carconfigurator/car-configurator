import {combineReducers} from 'redux';
import {connectRouter} from 'connected-react-router';

import {carsReducer} from './car';
import {configurationsReducer} from './configuration';
import {companiesReducer} from './company';
import {modelsReducer} from './model';
import {seriesReducer} from './series';
import {interfaceReducer} from './interface';
import {personsReducer} from './person';

export default history => combineReducers({
  router: connectRouter(history),
  carsReducer,
  companiesReducer,
  configurationsReducer,
  modelsReducer,
  seriesReducer,
  interfaceReducer,
  personsReducer
});

import React from 'react';
import Lottie from 'react-lottie';
import gears from './gears';
import styles from './Loader.module.scss';

const defaultOptions = {
  loop: true,
  autoplay: true,
  animationData: gears,
  rendererSettings: {
    preserveAspectRatio: 'xMidYMid slice'
  }
};

export const Loader = () => (
    <div className={styles.loader}>
      <Lottie
          options={defaultOptions}
          height={300}
          width={300}
          isStopped={false}
          isPaused={false}
      />
    </div>
);

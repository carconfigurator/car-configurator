import React, {PureComponent} from 'react';

import {BaseModal, ModalBody, ModalFooter, ModalHeader, Button, ButtonType} from 'components';

import infoIcon from 'assets/images/info_icon.png';

import styles from './InfoPanel.module.scss';

export default class InfoPanel extends PureComponent {
  state = {
    isOpen: false
  };

  onOpenModal = (e) => {
    e.stopPropagation();
    this.setState({isOpen: true});
  };

  onCloseModal = (e) => {
    e.stopPropagation();
    this.setState({isOpen: false});
  };

  render() {
    const {isOpen} = this.state;
    return (
        <React.Fragment>
          <img onClick={this.onOpenModal} alt="info-icon" src={infoIcon} className={styles.icon}/>
          <BaseModal
              isOpen={isOpen}
              onRequestClose={this.onCloseModal}
              className={styles.modal}
          >
            <ModalHeader>
              Сравните Ваш выбор с автомобилем
            </ModalHeader>
            <ModalBody>
              {this.props.children}
            </ModalBody>
            <ModalFooter>
              <Button type={ButtonType.Primary} onClick={this.onCloseModal}>Закрыть</Button>
            </ModalFooter>
          </BaseModal>
        </React.Fragment>
    );
  }
}

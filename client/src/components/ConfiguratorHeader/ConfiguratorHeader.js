import React, {PureComponent} from 'react';
import {connect} from 'react-redux';
import {withRouter} from 'react-router-dom';

import {InterfaceActions, PersonActions} from 'actions';
import {defaultPage} from 'constant';
import {getHeaderText} from 'utils';

import styles from './ConfiguratorHeader.module.scss';

class ConfiguratorHeader extends PureComponent {
  goTo = (path) => () => {
    this.props.personActions.clearPersonChoose();
    this.props.interfaceActions.setActivePage(defaultPage);
    this.props.history.push(path);
  };

  render() {
    const {company} = this.props;

    if (!company.slug) {
      return null;
    }

    const headerText = getHeaderText(this.props.page);

    return (
        <React.Fragment>
          <div className={styles.container}>
            <div className={styles.title}>{headerText}</div>
            <img onClick={this.goTo(`/config/${company.slug}`)} alt="mark" src={`/${company.image.filename}`}
                 className={styles.mark}/>
          </div>
          <hr/>
        </React.Fragment>
    );
  }
}

const mapStateToProps = state => ({
  company: state.companiesReducer.activeCompany,
  page: state.interfaceReducer.page
});

const mapDispatchToProps = dispatch => ({
  interfaceActions: new InterfaceActions(dispatch),
  personActions: new PersonActions(dispatch)
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(ConfiguratorHeader));

import React from 'react';
import {connect} from 'react-redux';

import {InterfaceActions} from 'actions';
import {isEmpty} from 'utils';
import {Button, ButtonType, BaseModal, ModalFooter, ModalHeader, ModalBody} from 'components';

class ConfirmationModal extends React.Component {
  handleCloseModal = () => {
    this.props.interfaceActions.closeConfirmationModal();
  };

  handleConfirmModal = () => {
    const {confirmation, interfaceActions} = this.props;
    confirmation && confirmation.action();
    interfaceActions.closeConfirmationModal();
  };

  render() {
    const {confirmation} = this.props;

    if (isEmpty(confirmation)) {
      return null;
    }

    return (
        <BaseModal
            onRequestClose={this.handleCloseModal}
            isOpen={true}>
          <ModalHeader>
            {confirmation.title}
          </ModalHeader>
          <ModalBody>
            {confirmation.text}
          </ModalBody>
          <ModalFooter>
            <Button style={{marginRight: 5}} onClick={this.handleConfirmModal}
                    type={ButtonType.Primary}>{confirmation.actionTitle}</Button>
            {!confirmation.withoutCancel && <Button onClick={this.handleCloseModal} type={ButtonType.Secondary}>Отмена</Button>}
          </ModalFooter>
        </BaseModal>
    );
  }
}

const mapStateToProps = state => ({
  confirmation: state.interfaceReducer.confirmation
});

const mapDispatchToProps = dispatch => ({
  interfaceActions: new InterfaceActions(dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(ConfirmationModal);

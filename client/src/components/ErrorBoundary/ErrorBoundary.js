import React, {PureComponent} from 'react';
import {Button} from 'components';

export default class ErrorBoundary extends PureComponent {
  state = {
    hasError: false
  };

  onButtonClick = () => {
    this.setState({hasError: false});
    window.location.href = '/';
  };

  componentDidCatch(error, info) {
    console.log(error, 'error');
    console.log(info, 'info');
    this.setState({hasError: true});
  }

  render() {
    if (this.state.hasError) {
      return (
          <div>
            <p>Ой! Произошла ошибка :(</p>
            <Button onClick={this.onButtonClick}>
              Сообщить об ошибке
              <br/>
              И вернуться на главную
            </Button>
          </div>
      );
    }

    return this.props.children;
  }
}

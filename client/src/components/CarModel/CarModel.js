import React from 'react';
import classnames from 'classnames';

import {Button, ButtonType} from 'components';
import {getFormatPrice} from 'utils';

import styles from './CarModel.module.scss';

export const CarModel = props => {
  const {model: {image, name, startPrice, cars}, handleClick} = props;
  const carModelStyles = classnames({
    [styles.container]: true,
    [styles.passive]: !cars.length
  });
  return (
      <div onClick={cars.length ? handleClick : null} className={carModelStyles}>
        <img alt="car-model" src={`/${image.filename}`} className={styles.image}/>
        <span className={classnames(styles.text, styles.model)}>{name}</span>
        <span className={classnames(styles.text, styles.price)}>от {getFormatPrice(startPrice)}</span>
        <span className={classnames(styles.text, styles.quantity)}>{cars.length} в наличии</span>
        <div className={styles.button}>
          <Button type={ButtonType.Primary}>Выбрать</Button>
        </div>
      </div>
  );
};

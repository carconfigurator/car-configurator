import React from 'react';
import classnames from 'classnames';

import {getFormatPrice} from 'utils';

import styles from './CarEdition.module.scss';

export const CarEdition = props => {
  const {description, name, startPrice, image} = props.configuration;
  const {onClick} = props;
  return (
      <div className={styles.container}>

        <img alt="care-edition" src={`/${image.filename}`} className={styles.image}/>

        <div className={classnames(styles.title, styles.model)}>{name}</div>

        <div className={classnames(styles.title, styles.price)}>от {getFormatPrice(startPrice)}</div>

        <div className={styles.tooltip}>
          <ul>
            {description.map((data, index) => (
                <li key={index}>
                  {data}
                </li>
            ))}
          </ul>
          <div onClick={onClick} className={styles.link}>
            Конфигурировать
          </div>
        </div>

      </div>
  );
};

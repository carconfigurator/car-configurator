import React, {Component} from 'react';
import axios from 'axios';
import {withRouter} from 'react-router-dom';
import classnames from 'classnames';
import Carousel from 'nuka-carousel';

import styles from './DealerList.module.scss';

class DealerList extends Component {
  state = {
    dealers: []
  };

  componentDidMount() {
    axios.post('/api/getDealers').then((response) => {
      this.setState({dealers: response.data});
    });
  }

  goToDealer = (company) => () => {
    this.props.setCompanyData(company);
    this.props.history.push('/config/' + company.slug);
  };

  render() {
    const {dealers} = this.state;

    if (!dealers.length) {
      return null;
    }

    return (
        <>
          <span className={styles.header}>Дилеры</span>
          <div className={styles.body}>
            <Carousel
                autoplay
                dragging
                slidesToShow={1}
                wrapAround
            >
              {dealers.map((dealer, id) => (
                  <div className={styles.container} key={id}>
                    {dealer.image && <img alt="dealer" src={`/${dealer.image.filename}`} className={styles.image}/>}
                    <span className={classnames(styles.text, styles.name)}>{dealer.name}</span>
                    <span className={classnames(styles.text, styles.address)}>{dealer.address}</span>
                  </div>
              ))}
            </Carousel>
          </div>
        </>
    );
  }
}

export default withRouter(DealerList);

import React, {PureComponent} from 'react';
import {connect} from 'react-redux';
import {withRouter} from 'react-router-dom';
import classnames from 'classnames';

import {InterfaceActions} from 'actions';
import {statusBar} from 'constant';
import {isEmpty} from 'utils';

import styles from './StatusBar.module.scss';

class StatusBar extends PureComponent {
  displayName = 'StatusBar';

  handleClick = (url) => () => {
    if (isEmpty(this.props.series)) {
      return;
    }
    this.props.interfaceActions.setActivePage(url);
  };

  renderComponents() {
    return statusBar.map((page, index) => {
      const statusBarItemStyles = classnames({
        [styles.item]: true,
        [styles.item__active]: page.url === this.props.page,
        [styles.item__pointer]: !isEmpty(this.props.series),
        [styles.item__disabled]: isEmpty(this.props.series) && index !== 0
      });
      return (
          <div className={statusBarItemStyles} key={index} onClick={this.handleClick(page.url)}>
            <div className={styles.round}/>
            <div className={styles.title}>
              {page.title}
            </div>
          </div>
      );
    });
  }

  render() {
    const statusBarWidth = statusBar.filter((status) => status.url === this.props.page)[0].statusBarWidth + 'vw';
    return (
        <>
          <div className={styles.container}>
            <div className={styles.top}>
              {this.renderComponents()}
            </div>
          </div>
          <div className={styles.bottom}>
            <hr/>
            <hr style={{width: statusBarWidth}} className={styles.line}/>
          </div>
        </>
    );
  }
}

const mapStateToProps = state => ({
  page: state.interfaceReducer.page,
  series: state.seriesReducer.activeSeries
});

const mapDispatchToProps = dispatch => ({
  interfaceActions: new InterfaceActions(dispatch)
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(StatusBar));

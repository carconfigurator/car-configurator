import React from 'react';

import {OptionItem} from './OptionItem';

import styles from './Select.module.scss';

class Select extends React.Component {
  handleChange = (event) => {
    this.props.onChange(event.target.value);
  };

  render() {
    return (
        <select className={styles.container} value={this.props.value} onChange={this.handleChange}>
          <OptionItem value=""/>
          {this.props.children}
        </select>
    );
  }
}

export default Select;

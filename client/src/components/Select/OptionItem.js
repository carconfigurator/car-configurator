import React from 'react';

export const OptionItem = (props) => (
    <option value={props.value.toLowerCase()}>{props.value}</option>
);

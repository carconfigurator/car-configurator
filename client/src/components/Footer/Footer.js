import React from 'react';

import styles from './Footer.module.scss';

export const Footer = () => (
    <footer className={styles.container}>
      <div className={styles.bottom}>
        <p className={styles.mail}>
          Есть вопросы? <a style={{fontWeight: 'bold'}}
                           href="mailto:info@carconfigurator.ru">info@carconfigurator.ru</a>
        </p>
        <p className={styles.copyright}>© 2019 «‎CarConfigurator.ru»</p>
      </div>
    </footer>
);

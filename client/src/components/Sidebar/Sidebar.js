import React, {PureComponent} from 'react';
import {withRouter} from 'react-router-dom';
import {connect} from 'react-redux';
import classnames from 'classnames';

import {InterfaceActions, PersonActions} from 'actions';
import {pages, defaultPage} from 'constant';
import {getFormatPrice} from 'utils';

import styles from './Sidebar.module.scss';

class Sidebar extends PureComponent {
  state = {
    open: false,
    notification: false
  };

  changePage = (page) => () => {
    this.props.interfaceActions.setActivePage(page);
  };

  goToMain = () => {
    this.props.personActions.clearPersonChoose();
    this.props.interfaceActions.setActivePage(defaultPage);
    this.props.history.push(`/config/${this.props.company.slug}`);
  };

  componentWillReceiveProps(nextProps) {
    if (nextProps.personChoose !== this.props.personChoose && !this.state.open) {
      this.setState({
        notification: true
      });
    }
  };

  handleClick = () => {
    this.setState({
      open: !this.state.open,
      notification: false
    });
  };

  renderPersonChoose() {
    const suitableCarsLength = this.props.filteredCars.suitable.length;
    const companyName = this.props.company.name;
    const modelName = this.props.model.name;
    const {personChoose} = this.props;
    let autoPrice = this.props.personChoose.engines.length ? this.props.personChoose.engines[0].headerPrice : 0;
    let optionsPrice = 0;

    return (
        <div className={styles.body}>
          <div className={styles.item}>
            <div className={styles.header}>
              <span className={styles.title}>Ваш выбор</span>
              <span onClick={this.goToMain} className={classnames(styles.link, styles.pink)}>Изменить</span>
            </div>
            <div className={styles.content}>{`${companyName} ${modelName}`}</div>
          </div>
          {pages.map((page) => {
            return (
                personChoose[page].map((data, index) => {
                  optionsPrice += data.bodyPrice ? data.bodyPrice : 0;
                  return (
                      <div key={index} className={styles.item}>
                        <div className={styles.header}>
                          <span className={styles.title}>{data.category.name}</span>
                          <span onClick={this.changePage(page)}
                                className={classnames(styles.link, styles.blue)}>Редактировать</span>
                        </div>
                        <span className={styles.content}>{data.title}</span>
                      </div>
                  );
                }));
          })}

          {
            autoPrice !== 0 &&
            <div className={styles.item}>
              <div className={styles.header}>
                <span className={styles.title}>Автомобиль</span>
              </div>
              <span className={styles.content}>{getFormatPrice(autoPrice)}</span>
            </div>
          }

          {
            optionsPrice !== 0 &&
            <div className={styles.item}>
              <div className={styles.header}>
                <span className={styles.title}>Добавленные опции</span>
              </div>
              <span className={styles.content}>{getFormatPrice(optionsPrice)}</span>
            </div>
          }

          {
            autoPrice !== 0 &&
            <div className={styles.item}>
              <div className={styles.header}>
                <span className={styles.title}>Итого</span>
              </div>
              <span className={classnames(styles.content, styles.price)}>
                  {getFormatPrice(autoPrice + optionsPrice)}
                </span>
            </div>
          }

          <div className={styles.item}>
            <span className={styles.title}>*С выбранными параметрами в наличии {suitableCarsLength} авто</span>
          </div>
        </div>
    );
  };

  render() {
    const {open} = this.state;
    let notification;
    if (open) {
      notification = false;
    }
    const sidebarClass = classnames({
      [styles.container]: true,
      [styles.active]: open
    });
    const notificationClass = classnames({
      [styles.notification]: true,
      [styles.passive]: !this.state.notification || notification
    });
    return (
        <div className={sidebarClass}>
          <div onClick={this.handleClick} className={styles.slider}>
            <div className={notificationClass}/>
            <div className={styles.left}>
              <div className={classnames(styles.button, styles.back)}/>
            </div>
            <div className={styles.right}>
              <div className={classnames({
                [styles.button]: true,
                [styles.front]: true,
                [styles.halfOpacity]: open
              })}/>
            </div>
          </div>
          {this.renderPersonChoose()}
        </div>
    );
  };
}

const mapStateToProps = state => ({
  company: state.companiesReducer.activeCompany,
  filteredCars: state.carsReducer.filteredCars,
  model: state.modelsReducer.activeModel,
  personChoose: state.personsReducer.personChoose

});

const mapDispatchToProps = dispatch => ({
  interfaceActions: new InterfaceActions(dispatch),
  personActions: new PersonActions(dispatch)
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Sidebar));

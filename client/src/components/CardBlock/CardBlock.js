import React from 'react';

import styles from './CardBlock.module.scss';

export const CardBlock = props => (
    <div className={styles.container}>
      <div className={styles.titleWrapper}>
        <p className={styles.title}>{props.mainTitle}</p>
      </div>
      <div className={styles.childrenWrapper}>
        {props.children}
      </div>
    </div>
);

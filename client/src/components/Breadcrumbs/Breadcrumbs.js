import React, {PureComponent} from 'react';
import {withRouter} from 'react-router-dom';
import {connect} from 'react-redux';

import {Button, ButtonType} from 'components';
import {getBreadcrumbs} from 'constant';

import styles from './Breadcrumbs.module.scss';

class Breadcrumbs extends PureComponent {
  displayName = 'Breadcrumbs';

  changePage = (page) => () => {
    this.props.history.push(page);
  };

  render() {
    const {step} = this.props;
    const company = this.props.company ? this.props.company.slug : '';
    const model = this.props.model ? this.props.model.slug : '';
    const breadcrumbs = getBreadcrumbs(company, model);
    const stepArray = [...Array(step).keys()];
    return (
        <div className={styles.container}>
          <div className={styles.body}>
            {stepArray.map((data, i) => (
                <span className={styles.link} key={i}
                      onClick={this.changePage(breadcrumbs[i].href)}>{breadcrumbs[i].title}</span>
            ))}
          </div>
          {this.props.button ?
              <Button type={ButtonType.Secondary} onClick={this.props.buttonClick}>Продолжить</Button>
              :
              null
          }
        </div>
    );
  }
}

const mapStateToProps = state => ({
  company: state.companiesReducer.activeCompany,
  model: state.modelsReducer.activeModel
});

export default withRouter(connect(mapStateToProps, null)(Breadcrumbs));

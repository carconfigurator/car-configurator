import {BaseModal, ModalBody, ModalFooter, ModalHeader} from './BaseModal';

export {
  BaseModal,
  ModalHeader,
  ModalBody,
  ModalFooter
};

import React from 'react';
import Modal from 'react-modal';
import classnames from 'classnames';

import styles from './BaseModal.module.scss';

Modal.setAppElement('#root');

export const ModalHeader = (props) => (
    <div className={styles.header}>
      <h3>{props.children}</h3>
    </div>
);

export const ModalFooter = (props) => (
    <div className={styles.footer}>
      {props.children}
    </div>
);

export const ModalBody = (props) => (
    <div className={styles.body}>
      {props.children}
    </div>
);

export const BaseModal = (props) => (
    <Modal
        isOpen={props.isOpen}
        shouldCloseOnOverlayClick={true}
        shouldCloseOnEsc={true}
        onRequestClose={props.onRequestClose}
        className={classnames(styles.modal, props.className)}
    >
      {props.children}
    </Modal>
);

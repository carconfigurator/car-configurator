import React from 'react';
import styles from './RadioButton.module.scss';

export const RadioButton = (props) => (
    <div className={styles.radioButton}>
      {props.active ?
          <div className={styles.active}/>
          :
          null
      }
    </div>
);

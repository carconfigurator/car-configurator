import React, {PureComponent} from 'react';
import {YMaps, Map, Placemark} from 'react-yandex-maps';

import './style.css';

export default class YandexMap extends PureComponent {
  render() {
    const {dealer} = this.props;
    const coordinates = [dealer.location[1], dealer.location[0]];
    const mapState = {
      center: coordinates,
      zoom: 15,
      controls: ['zoomControl', 'fullscreenControl']
    };
    return (
        <YMaps>
          <Map
              width='100%'
              defaultState={mapState}
              modules={['control.ZoomControl', 'control.FullscreenControl']}
          >

            <Placemark
                modules={['geoObject.addon.balloon']}
                defaultGeometry={
                  coordinates}
                properties={{
                  hintContent: dealer.name,
                  balloonContentBody: dealer.address
                }}
            />
          </Map>
        </YMaps>
    );
  }
}

import React from 'react';
import RCSlider from 'rc-slider';

import 'rc-slider/assets/index.css';

export const Slider = (props) => (
    <RCSlider
        className={props.className}
        onChange={props.onChange}
        min={props.min}
        max={props.max}
        value={props.value}
        handleStyle={{borderColor: '#1a4369'}}
        trackStyle={{backgroundColor: '#1a4369'}}
    />
);

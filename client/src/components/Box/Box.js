import React from 'react';
import classnames from 'classnames';

import styles from './Box.module.scss';

export const Box = (props) => (
    <p className={classnames({
      [styles.box]: true,
      [styles.boxBorder]: props.border
    })}>
      {props.children}
    </p>
);

export const BoxHeader = (props) => (
    props.children
);

export const BoxBody = (props) => (
    <span className={styles.body}>
      {props.children}
    </span>
);

import React from 'react';
import classnames from 'classnames';
import {ButtonType} from 'components';

import styles from './Button.module.scss';

export const Button = props => {
  const buttonClassName = classnames({
    [styles.button]: true,
    [styles.primary]: props.type === ButtonType.Primary,
    [styles.secondary]: props.type === ButtonType.Secondary,
    [styles.disabled]: props.disabled,
    [props.className]: !!props.className
  });
  return (
      <button style={props.style} onClick={props.onClick} className={buttonClassName}>
                <span>
                    {props.children}
                </span>
      </button>
  );
};

Button.displayName = 'Button';

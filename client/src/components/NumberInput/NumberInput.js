import React from 'react';

import {Slider, TextInput} from 'components/index';

import styles from './NumberInput.module.scss';

export default class NumberInput extends React.Component {

  state = {
    stringValue: this.props.changeValue ? this.props.changeValue(this.props.value) : this.props.value,
    intValue: this.props.value,
    value: this.props.value,
    error: null
  };

  static getDerivedStateFromProps(props, state) {
    if (state.value !== props.value) {
      const regExp = new RegExp('^\\d+$');
      let isValid = regExp.test(props.value);
      if (props.value < props.min) {
        return {
          stringValue: props.changeValue ? props.changeValue(props.value) : props.value,
          error: `Значение должно быть больше, чем ${props.changeValue ? props.changeValue(props.min) : props.min}`,
          value: parseInt(props.value)
        };
      }

      if (props.max < props.value) {
        return {
          stringValue: props.changeValue ? props.changeValue(props.value) : props.value,
          error: `Значение должно быть меньше, чем ${props.changeValue ? props.changeValue(props.max) : props.max}`,
          value: parseInt(props.value)
        };
      }

      if (isValid) {
        return {
          stringValue: `${props.changeValue ? props.changeValue(props.value) : props.value}`,
          value: parseInt(props.value),
          error: null
        };
      }
    }
    return null;
  }

  handleChangeTextInput = (value, id) => {
    this.props.onChange(value.replace(/\D/g, ''), id);
  };

  render() {
    const {value, error, stringValue} = this.state;
    const {min, max, onChange, title} = this.props;
    return (
        <div className={styles.container}>
          {title && <p className={styles.title}>{title}</p>}
          <div className={styles.numberInput}>
            <TextInput
                error={error}
                onChange={this.handleChangeTextInput}
                value={stringValue}
            />
            <Slider
                className={styles.slider}
                min={min}
                max={max}
                onChange={onChange}
                value={value}
            />
          </div>
          <span className={styles.error}>{error}</span>
        </div>
    );
  }
}

import React, {PureComponent} from 'react';
import {connect} from 'react-redux';
import {withRouter} from 'react-router-dom';

import {CompanyActions} from 'actions';

import styles from './CompanyList.module.scss';

class CompanyList extends PureComponent {
  goToCar = (company) => () => {
    this.props.companyActions.setActiveCompany(company);
    this.props.history.push('/config/' + company.slug);
  };

  componentDidMount() {
    this.props.companyActions.getCompanies();
  }

  render() {
    const {companiesList} = this.props;

    return (
        <div className={styles.container} id="marks">
          <div className={styles.header}>
            Выберите марку авто, чтобы продолжить
          </div>
          <div className={styles.body}>
            {companiesList.data.map(company => (
                <div key={company._id} className={styles.carousel}>
                  <div onClick={this.goToCar(company)} className={styles.item}>
                    <img className={styles.image} src={`/${company.image.filename}`}
                         alt={company.name}/>
                  </div>
                </div>
            ))}
          </div>
        </div>
    );
  }
}

const mapStateToProps = state => ({
  companiesList: state.companiesReducer.companiesList
});

const mapDispatchToProps = (dispatch) => ({
  companyActions: new CompanyActions(dispatch)
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(CompanyList));

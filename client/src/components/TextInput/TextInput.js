import React from 'react';
import classnames from 'classnames';

import styles from './TextInput.module.scss';

export default class TextInput extends React.PureComponent {
  onChange = event => {
    const {id} = this.props;
    const value = event.target.value;
    return this.props.onChange(value, id);
  };

  render() {
    const {id, value, placeholder, type, error} = this.props;
    const inputClassnames = classnames({
      [styles.input]: true,
      [styles.inputError]: error
    });
    return (
        <div className={styles.container}>
          <input
              className={inputClassnames}
              id={id}
              type={type}
              value={value ? value : value === 0 ? 0 : ''}
              placeholder={placeholder}
              onChange={this.onChange}/>
        </div>
    );
  }
}

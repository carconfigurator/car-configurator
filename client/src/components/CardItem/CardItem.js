import React from 'react';

import {getFormatPrice} from 'utils';
import {RadioButton, InfoPanel} from 'components';
import exampleItemImage from 'assets/images/example-item.png';

import styles from './CardItem.module.scss';

export const CardItem = props => {
  const {cardData, isActive, enableRadioButton} = props;
  const imageStyle = !enableRadioButton ? {marginLeft: 0} : null;

  return (
      <div onClick={props.onClick} className={styles.container}>

        <div className={styles.header}>
          {enableRadioButton && <RadioButton active={isActive}/>}

          {cardData.image && cardData.image.filename ?
              <img style={imageStyle} alt="cardItem" src={`/${cardData.image.filename}`} className={styles.image}/>
              :
              <img alt="cardItem" src={exampleItemImage} className={styles.image}/>
          }
        </div>

        <div className={styles.infoContainer}>
          <div className={styles.mainInfo}>

            <div className={styles.title}>
              {cardData.title}
            </div>

            {cardData.headerPrice ?
                <div className={styles.price}>
                  от {getFormatPrice(cardData.headerPrice)}
                </div>
                :
                null
            }

            {
              cardData.modelDescription ?
                  <InfoPanel>
                    <React.Fragment>
                      {cardData.image && cardData.image.filename ?
                          <img alt="cardItem" src={`/${cardData.image.filename}`} className={styles.infoImage}/>
                          :
                          <img alt="cardItem" src={exampleItemImage} className={styles.infoImage}/>
                      }
                      <p>{cardData.modelDescription}</p>
                    </React.Fragment>
                  </InfoPanel>
                  :
                  null
            }

          </div>

          <div className={styles.description}>
            {cardData.fullDescription}
          </div>

          <div className={styles.restInfo}>
            {cardData.bodyPrice ?
                <div className={styles.price}>{getFormatPrice(cardData.bodyPrice)}</div>
                :
                !cardData.headerPrice ?
                    <div className={styles.price}>Без изменения цены</div>
                    :
                    null}
            {cardData.additionalData ? cardData.additionalData.map((data, index) => {
              return (
                  <div key={index} className={styles.optionsContainer}>
                    <div className={styles.optionsTitle}>{data.title}:
                    </div>
                    <div className={styles.optionsValue}>{data.value}</div>
                  </div>
              );
            }) : null}
          </div>
        </div>
      </div>
  );
};

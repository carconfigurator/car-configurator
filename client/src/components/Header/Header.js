import React from 'react';
import {withRouter} from 'react-router-dom';

import logo from 'assets/images/configuratorHeader/logo.png';
import styles from './Header.module.scss';

const LINKS = [
  {
    url: '/config',
    name: 'Конфигуратор'
  },
  {
    url: '/insurance',
    name: 'Страхование'
  },
  {
    url: '/credit',
    name: 'Кредит'
  },
  {
    url: '/admin',
    name: 'Войти'
  }
];

const Header = (props) => (
    <header className={styles.header}>
      <div onClick={() => props.history.push('/')} className={styles.logo}><
          img alt="logo" src={logo} className={styles.img}/>
      </div>
      <input className={styles.menuBtn} type="checkbox" id={styles.menuBtn}/>
      <label className={styles.menuIcon} htmlFor={styles.menuBtn}><span className={styles.navicon}/></label>
      <ul className={styles.menu}>
        {LINKS.map((link, id) => (
            <li key={id}><span onClick={() => props.history.push(link.url)}>{link.name}</span></li>
        ))}
      </ul>
      <hr/>
    </header>
);

export default withRouter(Header);

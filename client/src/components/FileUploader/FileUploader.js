import React, {useEffect} from 'react';
import {useDropzone} from 'react-dropzone';

import styles from './FileUploader.module.scss';

export default function Previews(props) {
  const {getRootProps, getInputProps} = useDropzone({
    accept: 'image/*',
    onDrop: acceptedFiles => {
      props.onDrop(acceptedFiles);
    }
  });

  const thumbs = props.files.map((file, fileId) => (
      <div className={styles.thumb} key={fileId}>
        <div className={styles.thumbInner}>
          <img
              alt="preview"
              src={file.preview}
              className={styles.img}
          />
        </div>
      </div>
  ));

  useEffect( () => {
    props.files.forEach(file => URL.revokeObjectURL(file.preview));
  }, [props.files]);

  return (
      <section className={styles.container}>
        <div {...getRootProps({className: styles.dropzone})}>
          <input {...getInputProps()} />
          <p>Перетащите сюда файлы или нажмите и выберите их</p>
        </div>
        <aside className={styles.thumbsContainer}>
          {thumbs}
        </aside>
      </section>
  );
}

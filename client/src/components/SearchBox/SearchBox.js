import React, {PureComponent} from 'react';
import classnames from 'classnames';

import searchLens from 'assets/images/searchLens.png';

import styles from './SearchBox.module.scss';

export default class SearchBox extends PureComponent {

  handleChange = (event) => {
    this.props.onChange(event.target.value);
  };

  render() {
    const inputStyles = classnames({
      [styles.input]: true,
      [styles.input__active]: this.props.searchValue
    });

    return (
        <div className={styles.container}>
          <img alt="search-lens" src={searchLens} className={styles.lens}/>
          <input
              className={inputStyles}
              onChange={this.handleChange}
              value={this.props.searchValue}
              placeholder="Поиск..."
          />
        </div>
    );
  }
}

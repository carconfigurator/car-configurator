import React, {PureComponent} from 'react';
import ReactTable from 'react-table';
import 'react-table/react-table.css';

import {InfoPanel} from 'components';
import {getFormatPhone, getFormatPrice} from 'utils';
import {pages} from 'constant';

import styles from './CarFinal.module.scss';

export default class CarFinal extends PureComponent {
  getTableInfo = () => {
    const {cardData, personChoose} = this.props;
    const cutPersonChoose = {
      ...personChoose
    };
    let tableRows = [];
    pages.forEach((page) => {
      cardData[page].forEach((item, index) => {
        let tableRow = {
          dealerCar: item.title,
          personCarChoose: ''
        };
        personChoose[page].forEach((personItem, id) => {
          if (personItem.category._id === item.category) {
            tableRow.personCarChoose = personItem.title;
            cutPersonChoose[page].splice(id, 1);
          }
        });
        tableRows.push(tableRow);
      });
      cutPersonChoose[page].forEach(ex => {
        tableRows.push({
          dealerCar: '',
          personCarChoose: ex.title
        });
      });
    });
    return tableRows;
  };

  render() {
    const {cardData, personChoose, onClick} = this.props;
    let tableData;
    if (personChoose) {
      tableData = this.getTableInfo();
    }
    const imageCar = cardData.image ? cardData.image.filename : this.props.staticImage ? this.props.staticImage.filename : '';

    return (
        <div className={styles.container}>
          <img alt="cardItem" src={`/${imageCar}`} className={styles.image}/>
          <div className={styles.body}>
            <div className={styles.headerInfo}>
              {`${cardData.company.name} ${cardData.series.name}`}
              {tableData &&
              <InfoPanel>
                <ReactTable
                    data={tableData}
                    columns={[
                      {
                        Header: 'Автомобиль',
                        accessor: 'dealerCar',
                        style: {whiteSpace: 'unset'}
                      },

                      {
                        Header: 'Ваш Выбор',
                        accessor: 'personCarChoose',
                        style: {whiteSpace: 'unset'}
                      }
                    ]}
                    showPagination={false}
                    defaultPageSize={tableData.length}
                    className="-striped -highlight"
                />
              </InfoPanel>
              }
            </div>
            <div className={styles.headerInfo}>
              Цена дилера: {getFormatPrice(cardData.price)}
            </div>
            {cardData.dealer &&
            <>
              <div className={styles.info}>
                {cardData.dealer.name}
              </div>
              <div className={styles.info}>
                {cardData.dealer.address}
              </div>
              <div className={styles.info}>
                {getFormatPhone(cardData.dealer.phone)}
              </div>
            </>
            }
            {onClick && <p onClick={this.props.onClick(cardData)} className={styles.geoPoint}>
              Перейти к карточке автомобиля
            </p>
            }
          </div>
        </div>
    );
  }
}

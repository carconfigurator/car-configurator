export const ActionTypes = {
  //Interface Actions
  SET_ACTIVE_PAGE: 'SET_ACTIVE_PAGE',
  OPEN_CONFIRMATION_MODAL: 'OPEN_CONFIRMATION_MODAL',
  CLOSE_CONFIRMATION_MODAL: 'CLOSE_CONFIRMATION_MODAL',

  //Companies Actions
  GET_COMPANIES_LIST: 'GET_COMPANIES_LIST',
  GET_COMPANY: 'GET_COMPANY',
  SET_COMPANY_DATA: 'SET_COMPANY_DATA',

  //Models Actions
  GET_MODELS_LIST: 'GET_MODELS_LIST',
  GET_MODEL: 'GET_MODEL',
  SET_MODEL_DATA: 'SET_MODEL_DATA',

  //Series Actions
  GET_SERIES_LIST: 'GET_SERIES_LIST',
  GET_SERIES: 'GET_SERIES',
  SET_SERIES_DATA: 'SET_SERIES_DATA',

  //Cars Actions
  GET_CARS_LIST: 'GET_CARS_LIST',
  FILTER_CARS: 'FILTER_CARS',
  SET_CARS_DATA: 'SET_CARS_DATA',
  SET_ACTIVE_CAR: 'SET_ACTIVE_CAR',

  FIND_CAR: 'FIND_CAR',
  EDIT_CAR: 'EDIT_CAR',
  CREATE_CAR: 'CREATE_CAR',
  DELETE_CAR: 'DELETE_CAR',

  //Configuration Actions
  GET_CONFIGURATION: 'GET_CONFIGURATION',

  //Persons Actions
  CLEAR_PERSON_CHOOSE: 'CLEAR_PERSON_CHOOSE',
  SET_PERSON_CHOOSE: 'SET_PERSON_CHOOSE',
  CHECK_PERSON_AUTHENTICATION: 'CHECK_PERSON_AUTHENTICATION',
  LOGIN_PERSON: 'LOGIN_PERSON'
};

export const EStatus = {
  'IDLE': 0,
  'START': 1,
  'ERROR': 2,
  'SUCCESS': 3
};

export const IDLE = '_IDLE';
export const BEGIN = '_BEGIN';
export const SUCCESS = '_SUCCESS';
export const FAILED = '_FAILED';

export const pages = ['engines', 'exterior', 'interior', 'options'];

export const getPage = {
  'engines': 'Двигатель',
  'exterior': 'Экстерьер',
  'interior': 'Интерьер',
  'options': 'Пакеты опций'
};

export const defaultPage = 'series';

export const statusBar = [
  {
    title: 'Комлектация',
    url: 'series',
    statusBarWidth: '24'
  },
  {
    title: 'Двигатель',
    url: 'engines',
    statusBarWidth: '40'
  },
  {
    title: 'Экстерьер',
    url: 'exterior',
    statusBarWidth: '57'
  },
  {
    title: 'Интерьер',
    url: 'interior',
    statusBarWidth: '73'
  },
  {
    title: 'Опции',
    url: 'options',
    statusBarWidth: '86'
  },
  {
    title: 'Итог',
    url: 'results',
    statusBarWidth: '100'
  }
];

export const getBreadcrumbs = (company, model) => (
    [
      {
        title: 'Марка',
        href: '/'
      },
      {
        title: 'Модель',
        href: `/config/${company}`
      },
      {
        title: 'Конфигуратор',
        href: `/config/${company}/${model}`
      }
    ]
);

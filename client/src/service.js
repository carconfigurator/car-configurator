import axios from 'axios';
import {pages} from 'constant';

const config = {
  withCredentials: true
};

const API_URL = `/api`;

/*const get = (url) => {
  return axios.get(url, config).then(response => {
    return response.data;
  });
};*/

const post = (url, data) => {
  return axios.post(url, data, config).then(response => {
    return response.data;
  });
};

export class API {

  static getCompaniesList = () => {
    return post(`${API_URL}/getCompanies`);
  };

  static findCompany = (company) => {
    return post(`${API_URL}/findCompany`, {company});
  };

  static getModelsList = (company) => {
    return post(`${API_URL}/getModels`, {company});
  };

  static findModel = (company, model) => {
    return post(`${API_URL}/findModel`, {
      company,
      model
    });
  };

  static getSeriesList = (model) => {
    return post(`${API_URL}/getSeries`, {model});
  };

  static findSeries = (model, series) => {
    return post(`${API_URL}/findSeries`, {
      model,
      series
    });
  };

  static getConfiguration = (series) => {
    return post(`${API_URL}/findConfiguration/`, {
      series
    });
  };

  static getCars = (filter) => {
    return post(`${API_URL}/getCars`, filter);
  };

  static findCar = (id) => {
    return post(`${API_URL}/findCar`, {
      id
    });
  };

  static checkPersonAuthentication = () => {
    return post(`${API_URL}/admin/authentication`);
  };

  static loginPerson = (username, password) => {
    return post(`${API_URL}/admin/signin`, {
      username,
      password
    });
  };

  static editCar = (car) => {
    let formData = new FormData();
    formData.append('name', car.name);
    formData.append('company', car.company);
    formData.append('model', car.model);
    formData.append('series', car.series);
    formData.append('price', car.price);

    pages.forEach(page => {
      car[page].forEach(item => formData.append(page, item));
    });

    if (car.images.length) {
      formData.append('images', 'upload:CloudinaryImages-images-1001');
      car.images.forEach(image => {
        formData.append('CloudinaryImages-images-1001', image);
      });
    }

    return post(`/keystone/api/cars/${car._id}`, formData, {
      headers: {
        'Content-Type': 'multipart/form-data; charset=UTF-8'
      }
    });
  };

  static createCar = (car) => {
    let formData = new FormData();
    formData.append('name', car.name);
    formData.append('company', car.company);
    formData.append('model', car.model);
    formData.append('series', car.series);
    formData.append('price', car.price);

    pages.forEach(page => {
      car[page].forEach(item => formData.append(page, item));
    });

    if (car.images.length) {
      formData.append('images', 'upload:CloudinaryImages-images-1001');
      car.images.forEach(image => {
        formData.append('CloudinaryImages-images-1001', image);
      });
    }

    return post(`/keystone/api/cars/create`, formData, {
      headers: {
        'Content-Type': 'multipart/form-data; charset=UTF-8'
      }
    });
  };

  static deleteCar = (id) => {
    return post(`${API_URL}/admin/deleteCar`, {
      id
    });
  };
}

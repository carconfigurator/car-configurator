import {dispatchAsyncRequest} from 'utils';
import {ActionTypes} from 'constant';
import {API} from 'service';

export class ModelActions {
  constructor(dispatch) {
    this.dispatch = dispatch;
  }

  getModels = (company) => {
    return dispatchAsyncRequest(
        this.dispatch,
        ActionTypes.GET_MODELS_LIST,
        () => API.getModelsList(company)
    );
  };

  setActiveModel = (activeModel) => {
    return this.dispatch({
      type: ActionTypes.SET_MODEL_DATA,
      payload: activeModel
    });
  };

  findModel = (company, model) => {
    return dispatchAsyncRequest(
        this.dispatch,
        ActionTypes.GET_MODEL,
        () => API.findModel(company, model)
    );
  };
}

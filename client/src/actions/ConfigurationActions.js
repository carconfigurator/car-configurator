import {dispatchAsyncRequest} from 'utils';
import {ActionTypes} from 'constant';
import {API} from 'service';

export class ConfigurationActions {
  constructor(dispatch) {
    this.dispatch = dispatch;
  }

  getConfiguration = (series) => {
    return dispatchAsyncRequest(
        this.dispatch,
        ActionTypes.GET_CONFIGURATION,
        () => API.getConfiguration(series)
    );
  };

  /*  setActiveCompany = (activeCompany) => {
      return this.dispatch({
        type: ActionTypes.SET_COMPANY_DATA,
        payload: activeCompany
      });
    };*/
}

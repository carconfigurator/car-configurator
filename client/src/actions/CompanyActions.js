import {dispatchAsyncRequest} from 'utils';
import {ActionTypes} from 'constant';
import {API} from 'service';

export class CompanyActions {
  constructor(dispatch) {
    this.dispatch = dispatch;
  }

  getCompanies = () => {
    return dispatchAsyncRequest(
        this.dispatch,
        ActionTypes.GET_COMPANIES_LIST,
        API.getCompaniesList
    );
  };

  setActiveCompany = (activeCompany) => {
    return this.dispatch({
      type: ActionTypes.SET_COMPANY_DATA,
      payload: activeCompany
    });
  };

  findCompany = (company) => {
    return dispatchAsyncRequest(
        this.dispatch,
        ActionTypes.GET_COMPANY,
        () => API.findCompany(company)
    );
  };
}

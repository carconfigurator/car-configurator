import {ActionTypes} from 'constant';

export class InterfaceActions {
  constructor(dispatch) {
    this.dispatch = dispatch;
  }

  openConfirmationModal = (title, text, actionTitle, action, withoutCancel = false) => {
    return this.dispatch({
      type: ActionTypes.OPEN_CONFIRMATION_MODAL,
      payload: {
        title,
        text,
        actionTitle,
        action,
        withoutCancel
      }
    });
  };

  closeConfirmationModal = () => {
    return this.dispatch({
      type: ActionTypes.CLOSE_CONFIRMATION_MODAL
    });
  };

  setActivePage = (page) => {
    return this.dispatch({
      type: ActionTypes.SET_ACTIVE_PAGE,
      payload: page
    });
  };
}

import {dispatchAsyncRequest} from 'utils';
import {ActionTypes} from 'constant';
import {API} from 'service';

export class SeriesActions {
  constructor(dispatch) {
    this.dispatch = dispatch;
  }

  getSeries = (model) => {
    return dispatchAsyncRequest(
        this.dispatch,
        ActionTypes.GET_SERIES_LIST,
        () => API.getSeriesList(model)
    );
  };

  setActiveSeries = (activeSeries) => {
    return this.dispatch({
      type: ActionTypes.SET_SERIES_DATA,
      payload: activeSeries
    });
  };

  findSeries = (model) => {
    return dispatchAsyncRequest(
        this.dispatch,
        ActionTypes.GET_SERIES,
        () => API.findSeries(model)
    );
  };
}

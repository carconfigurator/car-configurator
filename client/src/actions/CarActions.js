import {dispatchAsyncRequest} from 'utils';
import {ActionTypes} from 'constant';
import {API} from 'service';

export class CarActions {
  constructor(dispatch) {
    this.dispatch = dispatch;
  }

  getCars = (filter) => {
    return dispatchAsyncRequest(
        this.dispatch,
        ActionTypes.GET_CARS_LIST,
        () => API.getCars(filter)
    );
  };

  findCar = (id) => {
    return dispatchAsyncRequest(
        this.dispatch,
        ActionTypes.FIND_CAR,
        () => API.findCar(id)
    );
  };

  filterCars = (filteredCars) => {
    return this.dispatch({
      type: ActionTypes.FILTER_CARS,
      payload: filteredCars
    });
  };

  setActiveCar = (activeCar) => {
    return this.dispatch({
      type: ActionTypes.SET_ACTIVE_CAR,
      payload: activeCar
    });
  };

  editCar = (car) => {
    return dispatchAsyncRequest(
        this.dispatch,
        ActionTypes.EDIT_CAR,
        () => API.editCar(car)
    );
  };

  createCar = (car) => {
    return dispatchAsyncRequest(
        this.dispatch,
        ActionTypes.CREATE_CAR,
        () => API.createCar(car)
    );
  };

  deleteCar = (id) => {
    return dispatchAsyncRequest(
        this.dispatch,
        ActionTypes.DELETE_CAR,
        () => API.deleteCar(id)
    );
  }
}

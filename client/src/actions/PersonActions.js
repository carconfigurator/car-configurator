import {ActionTypes} from 'constant';
import {dispatchAsyncRequest} from '../utils';
import {API} from 'service';

export class PersonActions {
  constructor(dispatch) {
    this.dispatch = dispatch;
  }

  checkPersonAuthentication = () => {
    return dispatchAsyncRequest(
        this.dispatch,
        ActionTypes.CHECK_PERSON_AUTHENTICATION,
        API.checkPersonAuthentication
    );
  };

  clearPersonChoose = () => {
    return this.dispatch({
      type: ActionTypes.CLEAR_PERSON_CHOOSE
    });
  };

  setPersonChoose = (personChoose) => {
    return this.dispatch({
      type: ActionTypes.SET_PERSON_CHOOSE,
      payload: personChoose
    });
  };

  loginPerson = (username, password) => {
    return dispatchAsyncRequest(
        this.dispatch,
        ActionTypes.LOGIN_PERSON,
        () => API.loginPerson(username, password)
    );
  };
}

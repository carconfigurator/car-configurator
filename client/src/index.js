import React, {lazy, Suspense, Fragment} from 'react';
import ReactDOM from 'react-dom';
import {Provider} from 'react-redux';
import {Route, Switch, Redirect} from 'react-router';
import {ConnectedRouter} from 'connected-react-router';

import store, {history} from 'store';
import {NotFound, Login, AdvertPage} from 'pages';
import {Loader, Header, CompanyList, Footer, ConfirmationModal} from 'components';
import {unregister} from './registerServiceWorker';

import './reset.scss';
import './index.scss';

const ConfiguratorHeader = lazy(() => import('./components/ConfiguratorHeader'));
const Home = lazy(() => import('./pages/Home'));
const Models = lazy(() => import('./pages/Models'));
const Configurator = lazy(() => import('./pages/Configurator'));
const AdminPage = lazy(() => import('./pages/AdminPage'));
const Car = lazy(() => import('./pages/CarPage'));
const EditPage = lazy(() => import('./pages/EditPage'));
const CreditPage = lazy(() => import('./pages/CreditPage'));
const InsurancePage = lazy(() => import('./pages/InsurancePage'));

const WithHeader = ({component: Component}) => (
    <Fragment>
      <ConfiguratorHeader/>
      <Component/>
    </Fragment>
);

const checkAuth = () => {
  return store.getState().personsReducer.personAuthentication.data.success;
};

const PrivateRoute = ({component: Component, ...rest}) => (
    <Route
        {...rest}
        render={props =>
            checkAuth() ? (
                <Component {...props} />
            ) : (
                <Redirect
                    to={{
                      pathname: '/auth/login 18/0A 28/2',
                      state: {from: props.location}
                    }}
                />
            )
        }
    />
);

ReactDOM.render(
    <Provider store={store}>

      <ConnectedRouter history={history}>

        <Suspense fallback={<Loader/>}>

          <ConfirmationModal/>

          <Header/>

          <Switch>

            <Route exact path="/">
              <Home/>
            </Route>

            <Route exact path="/config" component={CompanyList}/>

            <Route exact path="/config/:company/">
              <WithHeader component={Models}/>
            </Route>

            <Route exact path="/config/:company/:model">
              <WithHeader component={Configurator}/>
            </Route>

            <Route exact path="/config/:company/:model/:id">
              <WithHeader component={Car}/>
            </Route>

            <Route exact path="/advertisement/" component={AdvertPage}/>

            <Route exact path="/auth/login" component={Login}/>

            <Route exact path="/credit">
              <CreditPage/>
            </Route>

            <Route exact path="/insurance">
              <InsurancePage/>
            </Route>

            <PrivateRoute exact component={AdminPage} path="/admin"/>

            <PrivateRoute exact component={EditPage} path="/admin/edit/:carId"/>

            <PrivateRoute exact component={EditPage} path="/admin/create"/>

            <Route>
              <NotFound/>
            </Route>

          </Switch>

          <Footer/>

        </Suspense>

      </ConnectedRouter>

    </Provider>,
    document.getElementById('root'));
unregister();

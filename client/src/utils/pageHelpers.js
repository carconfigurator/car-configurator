export const getHeaderText = (page) => {
  let headerText = '';
  switch (page) {
    case 'results':
      headerText = 'Ваш автомобиль';
      break;
    case 'series':
      headerText = 'Выберите модель';
      break;
    default:
      headerText = 'Выберите конфигурацию будущего авто';
  }
  return headerText;
};

export const isEmpty = (obj) => {
  if (!obj) {
    return true;
  }
  return Object.keys(obj).length === 0;
};

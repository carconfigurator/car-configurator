import {pages} from 'constant';
import exampleItem from 'assets/images/example-item.png';
import {isEmpty} from 'utils';

export const filterCars = (personChoose, cars) => {
  const {itemsId} = personChoose;
  let filteredCars = {
    suitable: [],
    similar: []
  };

  cars.forEach((car) => {
    let i = 0;
    pages.forEach((page) => {
      car[page].findIndex(option => itemsId.findIndex(itemId => {
        if (itemId === option._id) {
          i++;
        }
        return null;
      }));
    });
    if (i === itemsId.length) {
      filteredCars.suitable.push(car);
    } else {
      filteredCars.similar.push(car);
    }
  });
  return filteredCars;
};

export const sortConfiguration = configuration => {
  if (isEmpty(configuration)) {
    return {};
  }
  let sortedData = configuration;
  pages.forEach((page) => {
    let sortedPage = [];
    configuration[page].forEach((item) => {
      const {additionalData, fullDescription, headerDescription, headerPrice, bodyPrice, name, slug, title, _id, category} = item;
      let sortedItem = {
        name: item.category ? item.category.name : '',
        category: page,
        uniqueItems: item.category ? item.category.uniqueItems : false,
        items: [{
          additionalData: additionalData,
          category: category,
          image: item.image || item.category.image || exampleItem,
          fullDescription: fullDescription,
          headerDescription: headerDescription,
          headerPrice: headerPrice,
          bodyPrice: bodyPrice,
          name: name,
          slug: slug,
          title: title,
          _id: _id
        }]
      };

      if (!sortedPage.length) {
        sortedPage.push(sortedItem);
      } else {
        for (let i = 0; i < sortedPage.length; i++) {
          if (sortedPage[i].name === sortedItem.name) {
            sortedPage[i].items.push(sortedItem.items[0]);
            break;
          } else {
            if (sortedPage.length === i + 1) {
              sortedPage.push(sortedItem);
              break;
            }
          }
        }
      }
      sortedData[page] = sortedPage;
    });
  });
  return sortedData;
};

export const getPersonChoose = (block, item, page, personChoose) => {
  let data = personChoose[page];
  let {itemsId} = personChoose;
  //Если это первый выбор пользователя и массив пустой, то просто пушим
  if (!data.length || !itemsId.length) {
    data.push(item);
    itemsId.push(item._id);
  } else {
    const indexData = data.findIndex(i => i._id === item._id);
    const indexId = itemsId.findIndex(i => i === item._id);
    if (indexData >= 0) {
      //Если пользователь нажал заново на выбранную опцию, поэтому удаляем её
      data.splice(indexData, 1);
      itemsId.splice(indexId, 1);
    } else if (block.category === 'engines') {
      //Перед тем как обнулить данные с двигателями, находим индекс в массиве и удаляем его
      const engineIndex = itemsId.findIndex(itemId => itemId === data[0]._id);
      if (engineIndex >= 0) {
        itemsId.splice(engineIndex, 1);
      }
      data = [];
      data.push(item);
      itemsId.push(item._id);
    } else if (block.uniqueItems) {
      //Если блок, ex..Цвет и обивка салона - уникальный, то проверяем прошлый массив на ключи
      let cutData = [];
      data.forEach((itemChoose) => {
        //Если ключи не совпадают
        if (itemChoose.category._id !== item.category._id) {
          cutData.push(itemChoose);
        }
        const ind = itemsId.findIndex(itemId => itemId === itemChoose._id);
        if (ind >= 0) {
          itemsId.splice(ind, 1);
        }
      });
      cutData.push(item);
      itemsId.push(item._id);
      data = cutData;
    } else {
      //Если по всем условиям прошлись и ничего не нашли, то просто добавляем новую опцию
      data.push(item);
      itemsId.push(item._id);
    }
  }
  return {
    ...personChoose,
    [page]: data
  };
};

import {EStatus, SUCCESS, FAILED, BEGIN, IDLE} from 'constant';
//import {InterfaceActions} from '../actions/InterfaceActions';
//import {openRoles} from '../actions/NavigationActions';

export function dispatchRequest(
    dispatch,
    asyncCall,
    withoutDispatchErrorAction
) {
  const p = asyncCall();
  p && p.catch((error) => {
    !withoutDispatchErrorAction &&
    notifyServerError(error, dispatch);
  });
  return p;
}

export function dispatchAsyncRequest(
    dispatch,
    actionType,
    asyncCall,
    payload,
    withoutDispatchErrorAction
) {
  dispatch({
    type: `${actionType}${BEGIN}`,
    payload
  });
  const p = asyncCall();
  p && p.then(
      dispatchRequestSuccess(actionType, dispatch),
      dispatchRequestFail(actionType, dispatch, withoutDispatchErrorAction)
  );
  return p;
}

export function dispatchRequestSuccess(
    actionType,
    dispatch
) {
  return (payload) => {
    dispatch({
      type: `${actionType}${SUCCESS}`,
      payload
    });
  };
}

export function dispatchRequestFail(
    actionType,
    dispatch,
    withoutDispatchErrorAction
) {
  return (error) => {
    !withoutDispatchErrorAction &&
    notifyServerError(error, dispatch, actionType);

    if (actionType) {
      dispatch({
        type: `${actionType}${FAILED}`,
        payload: null,
        error: error
      });
    }
  };
}

const notifyServerError = (error, dispatch, actionType) => {
  /*  const {text, code} = error;
    const status = error.status || (error.response && error.response.status);
    if (status != null) {
      //Если пришла ошибка со статусом, отличным от 200, то обрабатываем в зависимости от статуса.
      //const interfaceActions = new InterfaceActions(dispatch);
      let text;

      switch (status) {
        case 429:
          text = 'Сервер перегружен. Попробуйте повторить запрос позднее.';
          break;
        case 403:
          text = 'Произошла ошибка при получении привилегий. Обратитесь к администратору.';
          break;
        default:
          text = 'Произошла ошибка. Обратитесь к администратору.';
      }

      //interfaceActions.notifyError(text);
    } else {
      //Если пришла обычная серверная ошибка, то обрабатываем в зависимости от кода ошибки.
      switch (code) {
        case EServerError.ACCESS_DENIED:
          new InterfaceActions(dispatch).openConfirmationModal(
              'Недостаточно прав',
              text,
              'На главную страницу',
              () => dispatch(openRoles())
          );
          break;
        default:
          if (!text) {
            console.error(actionType ? actionType + ' error' : error.code + ' error');
          }
          new InterfaceActions(dispatch).notifyError(text);
      }
    }*/
};

export function isLoading(data) {
  return !!data && EStatus.START === data.status;
}

export function isIdle(data) {
  return !!data && EStatus.IDLE === data.status;
}

export const asyncReducer = (action, actionType, prevState, getInitState) => {
  let newState = null;
  switch (action.type) {
    case actionType + IDLE:
      newState = {...getInitState()};
      break;
    case actionType + BEGIN:
      newState = {
        ...getInitState(),
        status: EStatus.START
      };
      break;
    case actionType + SUCCESS:
      newState = {
        ...prevState,
        data: action.payload,
        status: EStatus.SUCCESS
      };
      break;
    case actionType + FAILED:
      newState = {
        ...getInitState(),
        ...prevState,
        error: action.error,
        status: EStatus.ERROR
      };
      break;
    default: {
      newState = prevState;
    }
  }

  return newState;
};

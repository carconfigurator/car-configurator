export const getFormatPrice = price => {
  if (!price) {
    return '';
  }
  return price.toString().replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ') + ' ₽';
};

export const getFormatPhone = phone => {
  if (!phone) {
    return '';
  }
  return phone.toString().replace(/(\d)+(\d\d\d)(\d\d\d)(\d\d)(\d\d)/, '+$1 ($2) $3-$4-$5');
};

import React, {PureComponent} from 'react';
import {connect} from 'react-redux';
import {withRouter} from 'react-router-dom';

import {Series, Options} from 'pages';

import {StatusBar} from 'components';

class Configurator extends PureComponent {
  render() {
    return (
        <React.Fragment>
          <StatusBar/>
          {
            this.props.page === 'series' ?
                <Series/>
                :
                <Options/>
          }
        </React.Fragment>
    );
  }
}

const mapStateToProps = state => ({
  page: state.interfaceReducer.page
});

export default withRouter(connect(mapStateToProps, null)(Configurator));

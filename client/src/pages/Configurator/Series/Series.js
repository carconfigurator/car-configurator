import React, {Component} from 'react';
import {connect} from 'react-redux';
import {withRouter} from 'react-router-dom';

import {CarEdition, Breadcrumbs} from 'components';
import {pages} from 'constant';
import {isEmpty} from 'utils';
import {CompanyActions, ModelActions, SeriesActions, InterfaceActions} from 'actions';

import styles from './Styles.module.scss';

class Series extends Component {
  async componentDidMount() {
    if (isEmpty(this.props.activeCompany)) {
      await this.props.companyActions.findCompany(this.props.match.params.company);
    }
    if (isEmpty(this.props.activeModel)) {
      await this.props.modelActions.findModel(this.props.activeCompany._id, this.props.match.params.model);
    }
    this.props.seriesActions.getSeries(this.props.activeModel._id);
  }

  handleClick = (series) => () => {
    this.props.seriesActions.setActiveSeries(series);
    this.props.interfaceActions.setActivePage(pages[0]);
  };

  render() {
    const {seriesList} = this.props;
    return (
        <div className={styles.container}>
          <div className={styles.header}>
            <Breadcrumbs step={3}/>
          </div>
          <div className={styles.cars}>
            {seriesList.data.map(seriesItem => (
                <CarEdition key={seriesItem._id} onClick={this.handleClick(seriesItem)} configuration={seriesItem}/>
            ))}
          </div>
        </div>
    );
  }
}

const mapStateToProps = state => ({
  activeCompany: state.companiesReducer.activeCompany,
  activeModel: state.modelsReducer.activeModel,
  seriesList: state.seriesReducer.seriesList
});

const mapDispatchToProps = dispatch => ({
  companyActions: new CompanyActions(dispatch),
  modelActions: new ModelActions(dispatch),
  seriesActions: new SeriesActions(dispatch),
  interfaceActions: new InterfaceActions(dispatch)
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Series));

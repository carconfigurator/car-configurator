import React, {PureComponent} from 'react';
import {connect} from 'react-redux';
import {withRouter} from 'react-router-dom';

import {Results} from 'pages';
import {filterCars, getPersonChoose, isEmpty} from 'utils';
import {Breadcrumbs, Sidebar, CardItem, CardBlock, Loader} from 'components';
import {
  CompanyActions,
  ModelActions,
  SeriesActions,
  CarActions,
  ConfigurationActions,
  PersonActions,
  InterfaceActions
} from 'actions';
import {statusBar} from 'constant';

import styles from './Options.module.scss';

class Options extends PureComponent {
  async componentDidMount() {
    if (isEmpty(this.props.activeCompany)) {
      await this.props.companyActions.findCompany(this.props.match.params.company);
    }
    if (isEmpty(this.props.activeModel)) {
      await this.props.modelActions.findModel(this.props.activeCompany._id, this.props.match.params.model);
    }
    if (isEmpty(this.props.activeSeries)) {
      await this.props.seriesActions.findSeries(this.props.activeModel._id, this.props.match.params.series);
    }
    this.props.configurationActions.getConfiguration(this.props.activeSeries._id);
    this.props.carActions.getCars({series: this.props.activeSeries._id});
  }

  checkPersonChoose = (block, item) => () => {
    const newPersonChoose = getPersonChoose(block, item, this.props.page, this.props.personChoose);
    this.props.personActions.setPersonChoose(newPersonChoose);

    const filteredCars = filterCars(this.props.personChoose, this.props.carsList.data);
    this.props.carActions.filterCars(filteredCars);
  };

  checkActive(cardItemData) {
    const cards = this.props.personChoose[this.props.page];
    let isActive = false;
    for (let i = 0; i < cards.length; i++) {
      if (cards[i]._id === cardItemData._id) {
        isActive = true;
        break;
      }
    }
    return isActive;
  }

  changePage = () => {
    const activePage = statusBar.findIndex(i => i.url === this.props.page) + 1;
    this.props.interfaceActions.setActivePage(statusBar[activePage].url);
  };

  renderScreen() {
    if (!this.props.activeConfiguration[this.props.page].length) {
      return (
          <p className={styles.not_found}>
            К сожалению, сюда ещё ничего не добавили :(
          </p>
      );
    }
    return (
        this.props.activeConfiguration[this.props.page].map((cardBlockData, index) => {
          return (
              <CardBlock
                  mainTitle={cardBlockData.name}
                  key={index}
              >
                {cardBlockData.items.map((cardItemData, index) => {
                  const isActive = this.checkActive(cardItemData);
                  return (
                      <CardItem
                          enableRadioButton
                          isActive={isActive}
                          onClick={this.checkPersonChoose(cardBlockData, cardItemData)}
                          key={index}
                          cardData={cardItemData}/>
                  );
                })}
              </CardBlock>
          );
        }));
  }

  render() {
    if (isEmpty(this.props.activeConfiguration) && this.props.personChoose) {
      return <Loader/>;
    }

    return (
        <div className={styles.container}>
          {this.props.page === 'results' ?
              <Results/>
              :
              <div className={styles.wrapper}>
                <div className={styles.header}>
                  <Breadcrumbs button buttonClick={this.changePage} step={3}/>
                </div>
                {this.renderScreen()}
              </div>
          }
          <Sidebar/>
        </div>
    );
  }
}

const mapStateToProps = state => ({
  activeConfiguration: state.configurationsReducer.activeConfiguration,
  activeCompany: state.companiesReducer.activeCompany,
  activeModel: state.modelsReducer.activeModel,
  activeSeries: state.seriesReducer.activeSeries,
  carsList: state.carsReducer.carsList,
  page: state.interfaceReducer.page,
  personChoose: state.personsReducer.personChoose
});

const mapDispatchToProps = dispatch => ({
  carActions: new CarActions(dispatch),
  configurationActions: new ConfigurationActions(dispatch),
  companyActions: new CompanyActions(dispatch),
  modelActions: new ModelActions(dispatch),
  interfaceActions: new InterfaceActions(dispatch),
  personActions: new PersonActions(dispatch),
  seriesActions: new SeriesActions(dispatch)

});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Options));

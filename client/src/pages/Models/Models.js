import React, {PureComponent} from 'react';
import {connect} from 'react-redux';
import {withRouter} from 'react-router-dom';

import {CarModel, Breadcrumbs} from 'components';
import {CompanyActions, ModelActions} from 'actions';
import {isEmpty} from 'utils';

import styles from './Models.module.scss';

class Marks extends PureComponent {
  async componentDidMount() {
    if (isEmpty(this.props.activeCompany)) {
      await this.props.companyActions.findCompany(this.props.match.params.company);
    }
    this.props.modelActions.getModels(this.props.activeCompany._id);
  }

  goToOptions = (model) => () => {
    this.props.modelActions.setActiveModel(model);
    this.props.history.push(`/config/${this.props.activeCompany.slug}/${model.slug}`);
  };

  render() {
    const {modelsList} = this.props;
    return (
        <React.Fragment>
          <Breadcrumbs step={2}/>
          <div className={styles.models}>
            {modelsList.data.map(model => (
                <CarModel handleClick={this.goToOptions(model)} key={model._id} model={model}/>
            ))}
          </div>
        </React.Fragment>

    );
  }
}

const mapStateToProps = state => ({
  activeCompany: state.companiesReducer.activeCompany,
  modelsList: state.modelsReducer.modelsList
});

const mapDispatchToProps = dispatch => ({
  companyActions: new CompanyActions(dispatch),
  modelActions: new ModelActions(dispatch)
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Marks));

import AdminPage from './AdminPage';
import AdvertPage from './AdvertPage';
import Configurator from './Configurator';
import CreditPage from './CreditPage';
import EditPage from './EditPage';
import Models from './Models';
import Home from './Home';
import InsurancePage from './InsurancePage';
import Login from './Login';
import NotFound from './NotFound';
import Results from './Results';
import Options from './Configurator/Options';
import Series from './Configurator/Series';

export {
  AdminPage,
  AdvertPage,
  Configurator,
  CreditPage,
  EditPage,
  Models,
  Home,
  InsurancePage,
  Login,
  NotFound,
  Results,
  Options,
  Series
};

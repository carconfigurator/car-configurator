import React, {PureComponent} from 'react';
import {connect} from 'react-redux';
import {withRouter} from 'react-router-dom';
import matchSorter from 'match-sorter';
/*import ReactTable from 'react-table';*/
import 'react-table/react-table.css';

import {CarActions, CompanyActions, InterfaceActions} from 'actions';
import {Button, ButtonType, CarFinal} from 'components';
import {getFormatPrice} from 'utils';

import styles from './AdminPage.module.scss';

class AdminPage extends PureComponent {

  getTableSubComponent = (row) => (
      <div style={{padding: '20px'}}>
        <p>
          <span className={styles.tableDescription}>Марка:</span> {row.original.company.name}</p>
        <p>
          <span className={styles.tableDescription}>Модель:</span> {row.original.model.name}</p>
        <p>
          <span className={styles.tableDescription}>Двигатель:</span> {row.original.engines[0].title}</p>
        <p>
          <span
              className={styles.tableDescription}>Интерьер:</span> {row.original.interior.map(int => int.title).join(',')}
        </p>
        <p>
          <span
              className={styles.tableDescription}>Экстерьер:</span> {row.original.exterior.map(ext => ext.title).join(',')}
        </p>
        <p>
          <span className={styles.tableDescription}>Цена:</span> {getFormatPrice(row.original.price)}</p>
      </div>
  );

  getTableColumns = () => ([
    {
      Header: 'Машина',
      columns: [
        {
          Header: 'Марка',
          id: 'company',
          accessor: 'company.name',
          filterMethod: (filter, rows) =>
              matchSorter(rows, filter.value, {keys: ['company']}),
          filterAll: true
        },
        {
          Header: 'Модель',
          id: 'model',
          accessor: 'model.name',
          filterMethod: (filter, rows) =>
              matchSorter(rows, filter.value, {keys: ['model']}),
          filterAll: true
        },
        {
          Header: 'Пакет',
          id: 'series',
          accessor: 'series.name',
          filterMethod: (filter, rows) =>
              matchSorter(rows, filter.value, {keys: ['series']}),
          filterAll: true
        },
        {
          Header: 'Кнопки',
          filterable: false,
          Cell: row => (
              <div>
                <Button onClick={() => this.handleEdit(row.original)}>Edit</Button>
              </div>
          )
        }

      ]
    },
    {
      Header: 'Цена',
      columns: [
        {
          Header: 'Age',
          accessor: 'age'
        },
        {
          Header: 'Over 21',
          accessor: 'age',
          id: 'over',
          Cell: ({value}) => (value >= 21 ? 'Yes' : 'No'),
          filterMethod: (filter, row) => {
            if (filter.value === 'all') {
              return true;
            }
            if (filter.value === 'true') {
              return row[filter.id] >= 21;
            }
            return row[filter.id] < 21;
          },
          Filter: ({filter, onChange}) =>
              <select
                  onChange={event => onChange(event.target.value)}
                  style={{width: '100%'}}
                  value={filter ? filter.value : 'all'}
              >
                <option value="all">Show All</option>
                <option value="true">Can Drink</option>
                <option value="false">Can't Drink</option>
              </select>
        }
      ]
    }
  ]);

  componentDidMount() {
    const {dealers} = this.props;
    this.props.carActions.getCars({dealers});
  }

  handleEdit = (row) => console.log(row);

  editCar = (car) => () => {
    this.props.carActions.setActiveCar(car);
    this.props.history.push(`/admin/edit/${car._id}`);
  };

  deleteCar = (car) => () => {
    this.props.interfaceActions.openConfirmationModal(
        'Удалить объявление',
        'Вы действительно хотите удалить это объявление?',
        'Удалить', () => this.props.carActions.deleteCar(car._id).then(() => {
          this.props.carActions.getCars();
        }));
  };

  render() {
    const {carsList} = this.props;
    return (
        <div className={styles.container}>
          <div>
            <Button onClick={() => this.props.history.push('/admin/create')}
                    type={ButtonType.Secondary}>Добавить</Button>
          </div>
          {/*          <ReactTable
              data={this.props.carsList.data}
              filterable
              defaultFilterMethod={(filter, row) =>
                  String(row[filter.id]) === filter.value}
              SubComponent={row => (
                  this.getTableSubComponent(row)
              )}
              columns={this.getTableColumns()}
              defaultPageSize={10}
              className="-striped -highlight"
          />*/}
          {carsList.data.map((car, carId) => (
              <React.Fragment key={carId}>
                <CarFinal cardData={car}/>
                <div className={styles.buttonContainer}>
                  <Button onClick={this.editCar(car)} style={{marginRight: 5}}
                          type={ButtonType.Primary}>Редктировать</Button>
                  <Button onClick={this.deleteCar(car)} type={ButtonType.Primary}>Удалить</Button>
                </div>
              </React.Fragment>
          ))}
        </div>
    );
  }
}

const mapStateToProps = state => ({
  carsList: state.carsReducer.carsList,
  dealers: state.personsReducer.personAuthentication.data.dealers
});

const mapDispatchToProps = dispatch => ({
  companyActions: new CompanyActions(dispatch),
  interfaceActions: new InterfaceActions(dispatch),
  carActions: new CarActions(dispatch)
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(AdminPage));

import React, {PureComponent} from 'react';
import {connect} from 'react-redux';
import {withRouter} from 'react-router-dom';

import {CardBlock, CarFinal} from 'components';
import {CarActions} from 'actions';

import styles from './Results.module.scss';

class Results extends PureComponent {
  renderCars = (cars) => (
      cars.map((car, index) => (
          <CarFinal
              key={index}
              staticImage={this.props.series.image}
              cardData={car}
              personChoose={this.props.personChoose}
              onClick={this.goToCarPage}
          />
      ))
  );

  goToCarPage = (activeCar) => () => {
    const {company, model} = this.props;
    this.props.carActions.setActiveCar(activeCar);
    this.props.history.push(`/config/${company.slug}/${model.slug}/${activeCar._id}`);
  };

  renderEmpty = (text) => (
      <p className={styles.not_found}>
        {text}
      </p>
  );

  render() {
    const {filteredCars} = this.props;
    return (
        <div className={styles.results}>
          <CardBlock mainTitle="Подходящие автомобили">
            {filteredCars.suitable.length ?
                this.renderCars(filteredCars.suitable)
                :
                this.renderEmpty('Подходящих автомобилей не найдено')
            }
          </CardBlock>
          <p>

          </p>
          <CardBlock mainTitle="Похожие автомобили">
            {filteredCars.similar.length ?
                this.renderCars(filteredCars.similar)
                :
                this.renderEmpty('Похожих автомобилей не найдено')
            }
          </CardBlock>
        </div>

    );
  }
}

const mapStateToProps = state => ({
  filteredCars: state.carsReducer.filteredCars,
  company: state.companiesReducer.activeCompany,
  model: state.modelsReducer.activeModel,
  series: state.seriesReducer.activeSeries,
  personChoose: state.personsReducer.personChoose
});

const mapDispatchToProps = dispatch => ({
  carActions: new CarActions(dispatch)
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Results));

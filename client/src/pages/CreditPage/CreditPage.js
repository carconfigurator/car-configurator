import React, {Component} from 'react';
import {connect} from 'react-redux';
import {withRouter} from 'react-router-dom';
import classnames from 'classnames';

import {getFormatPrice} from 'utils';
import {Box, BoxHeader, BoxBody, NumberInput, Button, ButtonType} from 'components';

import bmwPic from './img/bmw5.png';
import mercedesPic from './img/merdedesS.png';
import solaris from './img/solaris.png';

import styles from './CreditPage.module.scss';
import {InterfaceActions} from 'actions';

class CreditPage extends Component {
  state = {
    carPrice: 4000000,
    firstMoney: 1000000,
    creditLong: 5
  };

  onChange = (key) => (value) => {
    this.setState({[key]: value});
  };

  formatNumber = (num) => (
      Math.round((num * 100) / 100)
  );

  openModal = () => {
    this.props.interfaceActions.openConfirmationModal(
        'Ваша заявка отправлена',
        'Мы скоро с вами свяжемся, чтобы продолжить оформление кредита',
        'Ок', () => {}, true
    );
  }

  render() {
    const {carPrice, firstMoney, creditLong} = this.state;

    let principal = carPrice - firstMoney;
    let interest = 12 / 100 / 12;
    let payments = creditLong * 12;

    let x = Math.pow(1 + interest, payments);
    let monthly = (principal * x * interest) / (x - 1);

    return (
        <div className={styles.container}>
          <div className={styles.wrapper}>
            <h3>Онлайн консультация и&nbsp;одобрение кредита без визита в&nbsp;офис банка</h3>
          </div>

          <div className={styles.wrapper}>

            <div className={styles.boxContainer}>
              <Box border>
                <BoxHeader>
                  <b>Специальные предложения</b>
                </BoxHeader>
                <BoxBody>
                  Специальные ставки и большой выбор —
                  отечественные автомобили и иномарки, дорогие и бюджетные.
                </BoxBody>
              </Box>


              <Box border>
                <BoxHeader>
                  <b>Ставка ниже с КАСКО</b>
                </BoxHeader>
                <BoxBody>
                  Оформлять страховку не обязательно, но с КАСКО ставка по кредиту будет ниже.
                </BoxBody>
              </Box>

              <Box border>
                <BoxHeader>
                  <b>Оформим кредит за 30 минут</b>
                </BoxHeader>
                <BoxBody>
                  Всего по 2 документам: паспорт и права. Без надбавки за срочность.
                </BoxBody>
              </Box>

              <Box border>
                <BoxHeader>
                  <b>Без первого взноса</b>
                </BoxHeader>
                <BoxBody>
                  Выберите автомобиль, заключите кредитный договор с банком и машина ваша.
                </BoxBody>
              </Box>
            </div>

          </div>


          <div className={styles.greyBackground}>
            <div className={styles.wrapper}>
              <h3>Кредитный калькулятор</h3>
              <div className={styles.calculator}>

                <div className={styles.calculatorInput}>

                  <div className={styles.calculatorRow}>
                    <NumberInput
                        title="Стоймость автомобиля"
                        min={10000}
                        max={15000000}
                        onChange={this.onChange('carPrice')}
                        value={carPrice}
                        changeValue={getFormatPrice}
                    />
                  </div>

                  <div className={styles.calculatorRow}>
                    <NumberInput
                        title="Первоначальный взнос"
                        min={this.formatNumber((carPrice / 100) * 20)}
                        max={this.formatNumber(carPrice - (carPrice / 10))}
                        onChange={this.onChange('firstMoney')}
                        value={firstMoney}
                        changeValue={getFormatPrice}
                    />
                  </div>

                  <div className={styles.calculatorRow}>
                    <NumberInput
                        title="Срок кредита"
                        min={1}
                        max={20}
                        onChange={this.onChange('creditLong')}
                        value={creditLong}
                        changeValue={(value) => (`${value} лет`)}
                    />
                  </div>

                </div>

                <div className={styles.calculatorRow}>
                  <span>Кредитная ставка <b>12%</b></span>
                </div>

                <div className={styles.calculatorRow}>
                  <span>Ежемесячный кредит: <b>{getFormatPrice(this.formatNumber(monthly))}</b></span>
                </div>

                <div>
                  <img alt="car" className={
                    classnames({
                      [styles.img]: true,
                      [styles.imgDisabled]: carPrice > 2000000
                    })
                  } src={solaris}/>
                  <img alt="car" className={
                    classnames({
                      [styles.img]: true,
                      [styles.imgDisabled]: carPrice < 7000000
                    })
                  } src={mercedesPic}/>
                  <img alt="car" className={
                    classnames({
                      [styles.img]: true,
                      [styles.imgDisabled]: carPrice < 2000001 || carPrice > 6999999
                    })
                  } src={bmwPic}/>
                </div>

                <Button onClick={this.openModal} type={ButtonType.Primary}>Подать заявку на кредит</Button>


              </div>
            </div>
          </div>

        </div>
    );
  }
}

const mapStateToProps = state => ({});

const mapDispatchToProps = dispatch => ({
  interfaceActions: new InterfaceActions(dispatch)
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(CreditPage));

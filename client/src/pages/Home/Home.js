import React from 'react';
import {HashLink} from 'react-router-hash-link';

import {CompanyList} from 'components';
import logo from 'assets/images/logo.png';

import styles from './Home.module.scss';

export const Home = () => (
    <React.Fragment>

      <div className={styles.background + ' full-width'}>
        <div className={styles.container}>

          <div className={styles.header}>
            <img alt="logo" src={logo} className={styles.logo}/>
          </div>

          <div className={styles.body}>
            <div className={styles.title}>
              КОНФИГУРАТОР АВТО
            </div>
            <h1 className={styles.slogan}>
              Подберите <br/>
              идеальный <br/>
              автомобиль
            </h1>
          </div>

          <div className={styles.chevron}>
            <HashLink smooth to="/#marks">
              <div className={styles.chevron__bottom}/>
            </HashLink>
          </div>

        </div>
      </div>

      <CompanyList/>


      {/*      <div style={{ background: '#f2f4f6' }} className="full-width">
        <div className="main-content">
          <DealerList/>
        </div>
      </div>*/}


    </React.Fragment>
);

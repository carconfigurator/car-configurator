import React from 'react';
import {withRouter} from 'react-router-dom';
import {connect} from 'react-redux';
import Select from 'react-select';

import {
  CarActions,
  CompanyActions,
  ConfigurationActions,
  InterfaceActions,
  ModelActions,
  SeriesActions
} from 'actions';
import {CardBlock, CardItem, TextInput, Button, ButtonType, FileUploader} from 'components';
import {pages, getPage} from 'constant';
import {isEmpty} from 'utils';

import styles from './EditPage.module.scss';

class EditPage extends React.Component {
  state = {
    car: {
      company: null,
      dealer: null,
      engines: [],
      exterior: [],
      image: {},
      images: [],
      interior: [],
      model: null,
      name: '',
      options: [],
      price: 0,
      series: null,
      slug: ''
    }
  };

  fetchAllData() {
    const {activeCar} = this.props;
    if (!isEmpty(activeCar)) {
      this.props.modelActions.getModels(activeCar.company._id);
      this.props.seriesActions.getSeries(activeCar.model._id);
      this.props.configurationActions.getConfiguration(activeCar.series._id).then(() => {
        this.setState({
          car: activeCar
        });
      });
    }
    this.props.companyActions.getCompanies();
  }

  componentDidMount() {
    const {activeCar} = this.props;
    const {carId} = this.props.match.params;
    if (isEmpty(activeCar) && carId) {
      this.props.carActions.findCar(carId).then(() => {
        this.fetchAllData();
      });
    } else {
      this.fetchAllData();
    }
  }

  handleCompanyChange = (company) => {
    this.setState(prevState => ({
      car: {
        ...prevState.car,
        company,
        model: null,
        series: null
      }
    }));
    company && company._id && this.props.modelActions.getModels(company._id);
  };

  handleModelChange = (model) => {
    this.setState(prevState => ({
      car: {
        ...prevState.car,
        model,
        series: null
      }
    }));
    model && model._id && this.props.seriesActions.getSeries(model._id);
  };

  handleSeriesChange = (series) => {
    this.setState(prevState => ({
      car: {
        ...prevState.car,
        series
      }
    }));
    series && series._id && this.props.configurationActions.getConfiguration(series._id);
  };

  handleCardItemChange = (block, item, page) => () => {
    const {car} = this.state;
    let pageData = [
      ...car[page]
    ];

    //Если это первый выбор пользователя и массив пустой, то просто пушим
    if (!pageData.length) {
      pageData.push(item);
    } else {
      const indexData = pageData.findIndex(i => i._id === item._id);
      if (indexData >= 0) {
        //Если пользователь нажал заново на выбранную опцию, поэтому удаляем её
        pageData.splice(indexData, 1);
      } else if (block.category === 'engines') {
        //Перед тем как обнулить данные с двигателями, находим индекс в массиве и удаляем его

        pageData = [];
        pageData.push(item);
      } else if (block.uniqueItems) {
        //Если блок, ex..Цвет и обивка салона - уникальный, то проверяем прошлый массив на ключи
        let cutData = [];
        pageData.forEach((itemChoose) => {
          //Если ключи не совпадают
          if (itemChoose.category._id !== item.category._id) {
            cutData.push(itemChoose);
          }
        });
        cutData.push(item);
        pageData = cutData;
      } else {
        //Если по всем условиям прошлись и ничего не нашли, то просто добавляем новую опцию
        pageData.push(item);
      }
    }

    this.setState(prevState => ({
      car: {
        ...prevState.car,
        [page]: pageData
      }
    }));
  };

  checkIsActiveCardItem(cardItemData, page) {
    const {car} = this.state;
    const pageData = [
      ...car[page]
    ];
    return pageData.findIndex(i => i._id === cardItemData._id) + 1;
  }

  handleTextInputChange = (value, key) => {
    this.setState(prevState => ({
      ...prevState,
      car: {
        ...prevState.car,
        [key]: value
      }
    }));
  };

  saveCar = () => {
    const {car} = this.state;

    const customCar = {
      name: car.name,
      images: car.images,
      company: car.company._id,
      /*      dealer: car.dealer._id,*/
      image: car.image,
      model: car.model._id,
      series: car.series._id,
      _id: car._id,
      price: car.price
    };
    pages.forEach(page => {
      customCar[page] = [];
      car[page].forEach(item => customCar[page].push(item._id));
    });

    if (this.props.location.pathname.includes('admin/create')) {
      this.props.carActions.createCar(customCar).then(() => {
        this.props.history.goBack();
      });
    } else {
      this.props.carActions.editCar(customCar).then(() => {
        this.props.history.goBack();
      });
    }
  };

  openModal = () => {
    this.props.interfaceActions.openConfirmationModal(
        'Сохранить изменения',
        'Вы действительно хотите сохранить изменения и выйти?',
        'Сохранить', this.saveCar);
  };

  renderConfiguration() {
    if (isEmpty(this.state.car.series)) {
      return null;
    }

    return pages.map((page, pageId) => {
          if (!this.props.activeConfiguration[page].length) {
            return null;
          }
          return (
              <React.Fragment key={pageId}>
                <h2 className={styles.pageTitle}>{getPage[page]}</h2>
                {this.props.activeConfiguration[page].map((cardBlockData, index) => (
                    <CardBlock
                        mainTitle={cardBlockData.name}
                        key={index}
                    >
                      {cardBlockData.items.map((cardItemData, index) => {
                        const isActive = this.checkIsActiveCardItem(cardItemData, page);
                        return (
                            <CardItem
                                onClick={this.handleCardItemChange(cardBlockData, cardItemData, page)}
                                enableRadioButton
                                isActive={isActive}
                                key={index}
                                cardData={cardItemData}/>
                        );
                      })}
                    </CardBlock>
                ))}
              </React.Fragment>
          );
        }
    );
  }

  onDrop = (acceptedFiles) => {
    acceptedFiles.map(file => Object.assign(file, {
      preview: URL.createObjectURL(file)
    }));

    this.setState(prevState => ({
      car: {
        ...prevState.car,
        images: [
          ...prevState.car.images,
          ...acceptedFiles
        ]
      }
    }));
  };

  render() {
    const {car} = this.state;
    const {companiesList, modelsList, seriesList} = this.props;

    return (
        <>
          <Select
              className={styles.select}
              isClearable
              placeholder="Марка"
              options={companiesList.data}
              onChange={this.handleCompanyChange}
              value={car.company}
              getOptionValue={(option) => option.slug}
              getOptionLabel={(option) => option.name}
          />
          <Select
              className={styles.select}
              isClearable
              isDisabled={!modelsList.data.length}
              placeholder="Модель"
              options={modelsList.data}
              onChange={this.handleModelChange}
              value={car.model}
              getOptionValue={(option) => option.slug}
              getOptionLabel={(option) => option.name}
          />
          <Select
              className={styles.select}
              isClearable
              isDisabled={!seriesList.data.length}
              placeholder="Серия"
              options={seriesList.data}
              onChange={this.handleSeriesChange}
              value={car.series}
              getOptionValue={(option) => option.slug}
              getOptionLabel={(option) => option.name}
          />
          {this.props.activeConfiguration && this.renderConfiguration()}
          <TextInput id="price" onChange={this.handleTextInputChange} value={this.state.car.price}/>
          <TextInput id="name" onChange={this.handleTextInputChange} value={this.state.car.name}/>

          <FileUploader files={this.state.car.images} onDrop={this.onDrop}/>

          <div className={styles.buttonGroup}>
            <Button type={ButtonType.Primary} style={{marginRight: 5}} onClick={this.openModal}>Сохранить</Button>
            <Button type={ButtonType.Secondary} onClick={this.props.history.goBack}>Назад</Button>
          </div>
        </>
    );
  }
}

const mapStateToProps = state => ({
  activeConfiguration: state.configurationsReducer.activeConfiguration,
  activeCar: state.carsReducer.activeCar,
  carsList: state.carsReducer.carsList,
  companiesList: state.companiesReducer.companiesList,
  modelsList: state.modelsReducer.modelsList,
  seriesList: state.seriesReducer.seriesList
});

const mapDispatchToProps = dispatch => ({
  carActions: new CarActions(dispatch),
  configurationActions: new ConfigurationActions(dispatch),
  companyActions: new CompanyActions(dispatch),
  interfaceActions: new InterfaceActions(dispatch),
  modelActions: new ModelActions(dispatch),
  seriesActions: new SeriesActions(dispatch)
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(EditPage));

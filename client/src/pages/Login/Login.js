import React, {Component} from 'react';
import {connect} from 'react-redux';
import {withRouter} from 'react-router-dom';
import {Redirect} from 'react-router';

import {Button, ButtonType, TextInput} from 'components';
import {PersonActions} from 'actions';

import styles from './Login.module.scss';

class Login extends Component {
  state = {redirectToReferrer: false};

  componentDidMount() {
    this.checkAuthentication();
  }

  checkAuthentication = () => {
    this.props.personActions.checkPersonAuthentication().then(() => {
      if (this.props.personAuthentication.data.success) {
        this.setState({
          redirectToReferrer: true
        });
      }
    });
  };

  login = () => {
    this.props.personActions.loginPerson(this.state.username, this.state.password).then(() => {
      this.checkAuthentication();
    });
  };

  onChangeText = (value, id) => {
    this.setState({[id]: value});
  };

  render() {
    let {from} = this.props.location.state || {from: {pathname: '/'}};
    let {redirectToReferrer} = this.state;

    if (redirectToReferrer) {
      return <Redirect to={from}/>;
    }

    return (
        <div className={styles.textBox}>
          <TextInput
              id="username"
              placeholder="Почта"
              value={this.state.username}
              onChange={this.onChangeText}
          />
          <TextInput
              id="password"
              placeholder="Пароль"
              type="password"
              value={this.state.password}
              onChange={this.onChangeText}
          />
          <Button type={ButtonType.Primary} onClick={this.login}>Войти</Button>
        </div>
    );
  }
}

const mapStateToProps = state => ({
  personAuthentication: state.personsReducer.personAuthentication
});

const mapDispatchToProps = dispatch => ({
  personActions: new PersonActions(dispatch)
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Login));

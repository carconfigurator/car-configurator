import React, {PureComponent} from 'react';
import {connect} from 'react-redux';
import {withRouter} from 'react-router-dom';
import Select from 'react-select';
import makeAnimated from 'react-select/animated';

import {CarActions, CompanyActions, ModelActions} from 'actions';
import {CarFinal} from 'components';

import styles from './AdvertPage.module.scss';


const animatedComponents = makeAnimated();

const Option = (props) => {
  return (
      <animatedComponents.Option {...props}>
        <div style={{
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'center'
        }}>
          <img style={{
            width: 15,
            height: 15,
            marginRight: 5
          }} src={`/${props.data.image.filename}`} alt="logo"/>
          {props.children}
        </div>
      </animatedComponents.Option>
  );
};

const SingleValue = (props) => {
  return (
      <animatedComponents.SingleValue {...props}>
        <div style={{
          display: 'flex',
          alignItems: 'center'
        }}>
          <img style={{
            width: 15,
            height: 15,
            marginRight: 5
          }} src={`/${props.data.image.filename}`} alt="logo"/>
          {props.children}
        </div>
      </animatedComponents.SingleValue>
  );
};

class AdvertPage extends PureComponent {
  state = {
    companies: [],
    filters: {
      company: null,
      model: null
    }
  };

  static getDerivedStateFromProps(props, state) {
    let companies = [];
    let models = [];
    props.companyList.data.forEach((company) => {
      companies.push({
        value: company.slug,
        label: company.name,
        id: company._id,
        image: company.image
      });
    });
    props.modelsList.data.forEach((model) => {
      models.push({
        value: model.slug,
        label: model.name,
        id: model._id
      });
    });
    return {
      companies,
      models
    };
  }

  componentDidMount() {
    this.props.carActions.getCars();
    this.props.companyActions.getCompanies();
  }

  goToCarPage = (activeCar) => () => {
    this.props.history.push(`/config/${activeCar.company.name.toLowerCase()}/${activeCar.model.name.toLowerCase()}/${activeCar._id}`);
  };

  handleCompanyChange = (company) => {
    this.setState({
      filters: {
        company
      }
    });
    company && company.value && this.props.modelActions.getModels(company.id);
  };

  handleModelChange = (model) => {
    this.setState({
      filters: {
        model
      }
    });
  };

  multiFilter = (arr, filters) => {
    const filterKeys = Object.keys(filters);
    return arr.filter(eachObj => {
      return filterKeys.every(eachKey => {

        if (!filters[eachKey] || !filters[eachKey].value) {
          return true;
        }

        return filters[eachKey].value.includes(eachObj[eachKey].name.toLowerCase());
      });
    });
  };

  render() {
    const {filters, companies, models} = this.state;
    const carsList = this.multiFilter(this.props.carsList.data, filters);
    return (
        <div>

          <div className={styles.selectContainer}>
            <Select
                isClearable
                components={{
                  ...animatedComponents,
                  Option,
                  SingleValue
                }}
                className={styles.select}
                placeholder="Марка"
                options={companies}
                onChange={this.handleCompanyChange}
                value={filters.company}
            />
            <Select
                isClearable
                className={styles.select}
                isDisabled={!models.length}
                placeholder="Модель"
                options={models}
                onChange={this.handleModelChange}
                value={filters.model}
            />
          </div>
          {carsList.map((car, id) => (
              <CarFinal onClick={this.goToCarPage} key={id} cardData={car}/>
          ))}
        </div>
    );
  }
}

const mapStateToProps = state => ({
  carsList: state.carsReducer.carsList,
  modelsList: state.modelsReducer.modelsList,
  companyList: state.companiesReducer.companiesList
});

const mapDispatchToProps = dispatch => ({
  carActions: new CarActions(dispatch),
  companyActions: new CompanyActions(dispatch),
  modelActions: new ModelActions(dispatch)
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(AdvertPage));

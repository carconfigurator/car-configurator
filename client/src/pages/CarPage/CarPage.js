import React, {PureComponent, lazy} from 'react';
import {connect} from 'react-redux';
import {withRouter} from 'react-router-dom';

import {NotFound} from 'pages';
import {CarActions, CompanyActions, ModelActions, SeriesActions} from 'actions';
import {isEmpty, getFormatPhone} from 'utils';
import {CardItem, CardBlock, CarFinal, Button, ButtonType} from 'components';

import exampleItem from 'assets/images/example-item.png';

import styles from './CarPage.module.scss';

const YandexMap = lazy(() => import('../../components/YandexMap'));

class CarPage extends PureComponent {
  componentDidMount() {
    if (isEmpty(this.props.car)) {
      this.props.carActions.findCar(this.props.match.params.id).then(async () => {
        const {car} = this.props;

        if (isEmpty(this.props.activeCompany)) {
          await this.props.companyActions.findCompany(car.company.name.toLowerCase());
        }
        if (isEmpty(this.props.activeModel)) {
          await this.props.modelActions.findModel(car.company._id, car.model.name.toLowerCase());
        }
        if (isEmpty(this.props.activeSeries)) {
          await this.props.seriesActions.findSeries(car.model._id, car.series.name.toLowerCase());
        }

      });
    }
  }

  getDealerInfo = () => {
    const {car: {dealer}} = this.props;
    return {
      title: dealer.name,
      image: dealer.image,
      fullDescription: dealer.address,
      bodyPrice: getFormatPhone(dealer.phone)
    };
  };

  render() {
    const {car, activeModel, activeSeries} = this.props;

    if (isEmpty(car)) {
      return <NotFound/>;
    }

    const carImage = car.image ? car.image.filename : activeSeries.image ? activeSeries.image.filename : activeModel.image ? activeModel.image.filename : exampleItem;

    const dealerInfo = this.getDealerInfo();

    return (
        <div className={styles.container}>

          <CardBlock mainTitle="Автомобиль">
            <div style={{
              display: 'flex',
              justifyContent: 'center'
            }}>
              <CarFinal staticImage={carImage} cardData={car}/>
            </div>
          </CardBlock>

          <CardBlock mainTitle="Двигатель">
            {car.engines.map((engine, engineId) => (
                <CardItem key={engineId} cardData={engine}/>
            ))}
          </CardBlock>

          <CardBlock mainTitle="Интерьерь">
            {car.interior.map((int, intId) => (
                <CardItem key={intId} cardData={int}/>
            ))}
          </CardBlock>


          <CardBlock mainTitle="Экстерьер">
            {car.exterior.map((ext, extId) => (
                <CardItem key={extId} cardData={ext}/>
            ))}
          </CardBlock>

          <CardBlock mainTitle="Дилер">
            <CardItem cardData={dealerInfo}/>
            <div className={styles.itemContainer}>
              <YandexMap dealer={car.dealer}/>
            </div>
          </CardBlock>

          <div className={styles.itemContainer}>
            <Button type={ButtonType.Primary} onClick={this.props.history.goBack}>Вернуться назад</Button>
          </div>

        </div>
    );
  }
}

const mapStateToProps = state => ({
  car: state.carsReducer.activeCar,
  activeCompany: state.companiesReducer.activeCompany,
  activeModel: state.modelsReducer.activeModel,
  activeSeries: state.seriesReducer.activeSeries
});

const mapDispatchToProps = dispatch => ({
  companyActions: new CompanyActions(dispatch),
  seriesActions: new SeriesActions(dispatch),
  modelActions: new ModelActions(dispatch),
  carActions: new CarActions(dispatch)
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(CarPage));

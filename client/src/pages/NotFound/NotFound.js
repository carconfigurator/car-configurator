import React, {Component} from 'react';
import {withRouter} from 'react-router-dom';

import {Button, ButtonType} from 'components';

import styles from './NotFound.module.scss';

class NotFound extends Component {
  handleClick = () => {
    this.props.history.push('/');
  };

  render() {
    return (
        <div className={styles.not_found}>
          <h1>404 Страница не найдена</h1>
          <h2>Упс... Кажется вы сконфигурировали что-то не то</h2>
          <h3>Попробуйте вернуться на главную страницу и собрать машину своей мечты</h3>
          <Button type={ButtonType.Primary} onClick={this.handleClick}>Перейти на главную</Button>
        </div>
    );
  }
}

export default withRouter(NotFound);

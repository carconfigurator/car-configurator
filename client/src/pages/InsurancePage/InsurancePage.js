import React, {useState} from 'react';
import classnames from 'classnames';

import {Box, BoxHeader, BoxBody} from 'components';
import kaskoImg from './img/kasko.png';

import InsuranceOSAGO from './InsuranceOSAGO';

import styles from './InsurancePage.module.scss';

const INSURANCE_TYPE = {
  KASKO: 'KASKO',
  OSAGO: 'OSAGO'
};

const InsuranceKASKO = () => (
    <div>
      <div className={styles.imageContainer}>
        <img className={styles.insuranceImage} src={kaskoImg} alt="insuranceType"/>
      </div>
      <p className={styles.textParagraph}>
        Наш эксперт должен осмотреть автомобиль, чтобы узнать цену <b>КАСКО</b>
      </p>
      <p className={styles.textParagraph}>
        Приблизительную стоймость Вы можете рассчитать с помощью наших экспертов
      </p>
    </div>
);

export const InsurancePage = () => {
  const [insurance, setInsurance] = useState(INSURANCE_TYPE.OSAGO);

  return (
      <div className={styles.container}>

        <div className={styles.wrapper}>
          <h3>Рассчитать КАСКО или ОСАГО 2019 года<br/>на&nbsp;онлайн-калькуляторе</h3>
        </div>

        <div className={styles.greyBackground}>
          <div className={styles.wrapper}>

            <h3>Для этого выполните 4 простых шага</h3>

            <div className={styles.boxContainer}>

              <Box>
                <BoxHeader>
                  Выберите автомобиль
                </BoxHeader>
              </Box>
              <Box>
                <BoxHeader>
                  Выберите&nbsp;<b>КАСКО</b>&nbsp;или&nbsp;<b>ОСАГО</b>
                </BoxHeader>
              </Box>
              <Box>
                <BoxHeader>
                  Оплата
                </BoxHeader>
              </Box>
              <Box>
                <BoxHeader>
                  Получения полиса
                </BoxHeader>
              </Box>

            </div>

          </div>
        </div>

        <div className={styles.wrapper}>
          <div className={styles.boxContainer}>

            <Box border>
              <BoxHeader>
                <b>Без лишних хлопот</b>
              </BoxHeader>
              <BoxBody>
                Ремонт стекол, фар и&nbsp;зеркал будет оплачен без комиссара и&nbsp;справок. А&nbsp;в&nbsp;случае ДТП
                можем предоставить бесплатный эвакуатор
              </BoxBody>
            </Box>

            <Box border>
              <BoxHeader>
                <b>Полис КАСКО без похода в&nbsp;офис</b>
              </BoxHeader>
              <BoxBody>
                Наш представитель привезёт оформленный полис и&nbsp;проведет осмотр авто в&nbsp;удобное вам дневное
                время и&nbsp;место
              </BoxBody>
            </Box>

            <Box border>
              <BoxHeader>
                <b>Профессиональная помощь 24/7</b>
              </BoxHeader>
              <BoxBody>
                Личный менеджер круглосуточно на&nbsp;связи и&nbsp;поможет вам при урегулировании и&nbsp;оформлении ДТП
              </BoxBody>
            </Box>

          </div>

        </div>


        <div className={styles.greyBackground}>

          <div className={styles.wrapper}>

            <h3>Выберите тип полиса</h3>

            <div className={styles.insuranceContainer}>
              <div className={styles.underlineBox}>
                <span
                    onClick={() => setInsurance(INSURANCE_TYPE.OSAGO)}
                    className={classnames({
                      [styles.customUnderline]: true,
                      [styles.customUnderlineActive]: insurance === INSURANCE_TYPE.OSAGO
                    })}>ОСАГО</span>
              </div>

              <div className={styles.underlineBox}>
                <span
                    onClick={() => setInsurance(INSURANCE_TYPE.KASKO)}
                    className={classnames({
                      [styles.customUnderline]: true,
                      [styles.customUnderlineActive]: insurance === INSURANCE_TYPE.KASKO
                    })}>КАСКО</span>
              </div>
            </div>

            {insurance === INSURANCE_TYPE.OSAGO && <InsuranceOSAGO/>}

            {insurance === INSURANCE_TYPE.KASKO && <InsuranceKASKO/>}

          </div>

        </div>


      </div>
  );

};

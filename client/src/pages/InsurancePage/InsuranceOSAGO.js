import React from 'react';
import {connect} from 'react-redux';
import Select from 'react-select';

import {CompanyActions, InterfaceActions, ModelActions, SeriesActions} from 'actions';
import osagoImg from 'pages/InsurancePage/img/osago.png';
import styles from './InsurancePage.module.scss';
import {getFormatPrice} from 'utils';
import {Button, ButtonType} from 'components/Button';

class InsuranceOSAGO extends React.Component {
  state = {
    company: null,
    model: null,
    series: null,
    osagoPrice: null,
    yearsList: [...Array(40).keys()].map(i => {
      return {
        value: i + 1980
      };
    })
  };

  componentDidMount() {
    this.props.companyActions.getCompanies();
  }

  handleCompanyChange = (company) => {
    this.setState({
      company,
      osagoPrice: null,
      model: null,
      series: null,
      year: null
    }, () => company && this.props.modelActions.getModels(company._id));
  };

  handleModelChange = (model) => {
    this.setState({
      model,
      series: null,
      year: null,
      osagoPrice: null
    }, () => model && this.props.seriesActions.getSeries(model._id));
  };

  handleSeriesChange = (series) => {
    this.setState({
      series,
      osagoPrice: null,
      year: null
    });
  };

  handleYearChange = (year) => {
    this.setState({
      year,
      osagoPrice: Math.round(Math.random() * (5000 - 2000) + 2000)
    });
  };

  openModal = () => {
    this.props.interfaceActions.openConfirmationModal(
        'Ваша заявка отправлена',
        'Мы скоро с вами свяжемся, чтобы продолжить оформление полиса ОСАГО',
        'Спасибо', () => {
        }, true
    );
  };

  render() {
    const {company, model, series, osagoPrice, year, yearsList} = this.state;
    const {companiesList, modelsList, seriesList} = this.props;

    return (
        <div>
          <div className={styles.imageContainer}>
            <img className={styles.insuranceImage} src={osagoImg} alt="insuranceType"/>
          </div>
          <p className={styles.textParagraph}>
            Заполните информацию о легковом автомобиле и узнайте стоймость полиса <b>ОСАГО</b>
          </p>
          <p className={styles.textParagraph}>
            Точную стоймость Вы можете рассчитать на форме ниже.
          </p>
          <div className={styles.calculator}>
            <Select
                isClearable
                className={styles.select}
                placeholder="Марка"
                options={companiesList.data}
                onChange={this.handleCompanyChange}
                value={company}
                getOptionValue={(option) => option.slug}
                getOptionLabel={(option) => option.name}
            />
            <Select
                isClearable
                className={styles.select}
                placeholder="Модель"
                options={modelsList.data}
                onChange={this.handleModelChange}
                value={model}
                getOptionValue={(option) => option.slug}
                getOptionLabel={(option) => option.name}
            />

            <Select
                isClearable
                className={styles.select}
                placeholder="Серия"
                options={seriesList.data}
                onChange={this.handleSeriesChange}
                value={series}
                getOptionValue={(option) => option.slug}
                getOptionLabel={(option) => option.name}
            />

            <Select
                isClearable
                className={styles.select}
                placeholder="Год автомобиля"
                options={yearsList}
                onChange={this.handleYearChange}
                value={year}
                getOptionValue={(option) => option.value}
                getOptionLabel={(option) => option.value}
            />

            {osagoPrice &&
            <>
              <h3 className={styles.textParagraph}>Стоимость полиса ОСАГО: {getFormatPrice(osagoPrice)}</h3>
              <Button onClick={this.openModal} type={ButtonType.Primary}>Подать заявку</Button>
            </>
            }

          </div>
        </div>
    );
  }
}

const mapStateToProps = state => ({
  companiesList: state.companiesReducer.companiesList,
  modelsList: state.modelsReducer.modelsList,
  seriesList: state.seriesReducer.seriesList
});

const mapDispatchToProps = dispatch => ({
  companyActions: new CompanyActions(dispatch),
  modelActions: new ModelActions(dispatch),
  seriesActions: new SeriesActions(dispatch),
  interfaceActions: new InterfaceActions(dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(InsuranceOSAGO);
